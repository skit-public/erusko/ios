//
//  BackgroundEntities.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import Foundation

enum BackgroundTaskIdentifier: String, CaseIterable {

    // only one task identifier is allowed have the .exposure-notification suffix
    case exposureNotification = "exposure-notification"
    case exposurePausedStateNotification = "exposure-paused-state"

    var schedulerIdentifier: String {
        guard let bundleID = Bundle.main.bundleIdentifier else { return "invalid-task-id!" }
        return "\(bundleID).\(rawValue)"
    }

}

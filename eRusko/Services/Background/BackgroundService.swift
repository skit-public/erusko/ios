//
//  BackgroundService.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import Foundation
import UIKit
import BackgroundTasks
import FirebaseFunctions
import FirebaseAnalytics

final class BackgroundService {

    let exposureService: ExposureServicing
    let reporter: ReportServicing
    private(set) var isRunning: Bool = false

    let taskIdentifier = BackgroundTaskIdentifier.exposureNotification
    let exposurePausedStateIdentifier = BackgroundTaskIdentifier.exposurePausedStateNotification

    init(exposureService: ExposureServicing, reporter: ReportServicing) {
        self.exposureService = exposureService
        self.reporter = reporter

        registerTasks()
    }

    func registerTasks() {
        Self.scheduleDeadmanNotification()
        BGTaskScheduler.shared.register(forTaskWithIdentifier: taskIdentifier.schedulerIdentifier, using: .main) { task in
            Log.log("BGTask: Start background exposure check")

            let progress = self.performTask(task)

            // Handle running out of time
            task.expirationHandler = {
                Log.log("BGTask: EXPIRATION HANDLER background exposure")
                self.scheduleBackgroundTaskIfNeeded(next: false)
                progress.cancel()
                task.setTaskCompleted(success: false)
            }

            // Schedule the next background task
            self.scheduleBackgroundTaskIfNeeded(next: true)
        }
        BGTaskScheduler.shared.register(forTaskWithIdentifier: exposurePausedStateIdentifier.schedulerIdentifier, using: .main) { task in
            Log.log("BGTask: Start background paused state check")

            let progress = self.performExposurePausedStateTask(task)

            // Handle running out of time
            task.expirationHandler = {
                Log.log("BGTask: EXPIRATION HANDLER background paused state")
                self.scheduleExposurePausedStateBackgroundTaskIfNeeded(next: false)
                progress.cancel()
                task.setTaskCompleted(success: false)
            }

            // Schedule the next background task
            self.scheduleExposurePausedStateBackgroundTaskIfNeeded(next: true)
        }
    }

    func performTask() {
        guard !isRunning else { return }
        _ = self.performTask(nil)
    }

    func scheduleBackgroundTaskIfNeeded(next: Bool = false) {
        BGTaskScheduler.shared.getPendingTaskRequests { (pendingTasks) in
            let task = pendingTasks.first(where: { $0.identifier == self.taskIdentifier.schedulerIdentifier })
            
            if let task = task {
                // Check if task is stucked in history too much and need to be refresh
                if let earlyDate = task.earliestBeginDate, (earlyDate + TimeInterval(24 * 60 * 60)) < Date() {
                    log("BG: rescheduling, becase task is stucked in history exposure")
                    self.scheduleBackgroundTask(next: next)
                    return
                }
                
                log("bg task \(self.taskIdentifier.schedulerIdentifier) is already sheduled for \(String(describing: task.earliestBeginDate?.toLocalString()))")
                return
            }
            
            self.scheduleBackgroundTask(next: next)
        }
    }
    
    func scheduleBackgroundTask(next: Bool = false) {

        let taskRequest = BGProcessingTaskRequest(identifier: self.taskIdentifier.schedulerIdentifier)
        taskRequest.requiresNetworkConnectivity = true
        taskRequest.requiresExternalPower = false
        if next {
            // start after next 6 hours
            let earliestBeginDate = Date(timeIntervalSinceNow: AppConfig.shared.exposuresData.diagnosisKeysPeriodicFrameHours * 60 * 60) // TODO: diagnosisKeysPeriodicFrameHours
            taskRequest.earliestBeginDate = earliestBeginDate
            Log.log("Background: Schedule exposure next task to: \(earliestBeginDate.toLocalString())")
        }

        do {
            try BGTaskScheduler.shared.submit(taskRequest)
            log("BG Task \(self.taskIdentifier.schedulerIdentifier) subimtted : \(String(describing: taskRequest.earliestBeginDate?.toLocalString()))")

        } catch {
            Log.log("Background: Unable to schedule exposure background task: \(error)")
        }
    }
    
    func scheduleExposurePausedStateBackgroundTaskIfNeeded(next: Bool = false) {
        BGTaskScheduler.shared.getPendingTaskRequests { (pendingTasks) in
            
            let task = pendingTasks.first(where: { $0.identifier == self.exposurePausedStateIdentifier.schedulerIdentifier })
            
            if let task = task {
                // Check if task is stucked in history too much and need to be refresh
                if let earlyDate = task.earliestBeginDate, (earlyDate + TimeInterval(24 * 60 * 60)) < Date() {
                    log("BG: rescheduling, becase task is stucked in history paused state")
                    self.scheduleExposurePausedStateBackgroundTask(next: next)
                    return
                }
                
                log("bg task \(self.exposurePausedStateIdentifier.schedulerIdentifier) is already sheduled for \(String(describing: task.earliestBeginDate?.toLocalString()))")
                return
            }
            
            self.scheduleExposurePausedStateBackgroundTask(next: next)
        }
    }
    
    func scheduleExposurePausedStateBackgroundTask(next: Bool = false) {

        let taskRequest = BGProcessingTaskRequest(identifier: self.exposurePausedStateIdentifier.schedulerIdentifier)
        taskRequest.requiresNetworkConnectivity = true
        taskRequest.requiresExternalPower = false
        if next {
            // start after next 4 hours
            let earliestBeginDate = Date(timeIntervalSinceNow: AppConfig.shared.exposuresData.selfCheckerPeriodHours * 60 * 60)
            taskRequest.earliestBeginDate = earliestBeginDate
            Log.log("Background: Schedule paused checker next task to: \(earliestBeginDate.toLocalString())")
        }

        do {
            try BGTaskScheduler.shared.submit(taskRequest)
            log("BG Task \(self.exposurePausedStateIdentifier.schedulerIdentifier) subimtted : \(String(describing: taskRequest.earliestBeginDate?.toLocalString()))")

        } catch {
            Log.log("Background: Unable to schedule paused state background task: \(error)")
        }
    }

    // MARK: - Bluetooth notification

    func showBluetoothOffUserNotificationIfNeeded() {
        let notificationCenter = UNUserNotificationCenter.current()

        // bundleIdentifier is defined in Info.plist and can never be nil!
        guard let bundleID = Bundle.main.bundleIdentifier else {
            Log.log("BGNotification: Could not access bundle identifier")
            return
        }
        let identifier = bundleID + ".notifications.bluetooth_off"

        if exposureService.authorizationStatus == .authorized, !exposureService.isBluetoothOn {
            let content = UNMutableNotificationContent()
            content.title = AppConfig.shared.localized.exposureErrorFailedBluetoothDisabledTitle!
            content.body = AppConfig.shared.localized.exposureErrorFailedBluetoothDisabledMessage!
            content.sound = .default

            let request = UNNotificationRequest(
                identifier: identifier,
                content: content,
                trigger: nil

            )
            notificationCenter.add(request) { error in
                DispatchQueue.main.async {
                    if let error = error {
                        Log.log("BGNotification: Error showing user notification \(error)")
                    }
                }
            }
        } else {
            notificationCenter.removeDeliveredNotifications(withIdentifiers: [identifier])
        }
    }

    // MARK: - Deadman notification

    /// Taken from: https://github.com/corona-warn-app/cwa-app-ios
    /// Schedules a local notification to fire 24 hours from now.
    /// In case the background execution fails  there will be a backup notification for the
    /// user to be notified to open the app. If everything runs smoothly,
    /// the current notification will always be moved to the future, thus never firing.
    static func scheduleDeadmanNotification(override: Bool = false) {
        // Check if Deadman Notification is already scheduled
        let notificationCenter = UNUserNotificationCenter.current()
        
        notificationCenter.getPendingNotificationRequests(completionHandler: { notificationRequests in
            if notificationRequests.contains(where: { $0.identifier == Self.deadmanNotificationIdentifier }),
               override == false {
                // Deadman Notification already setup -> return
                return
            } else {
                // No Deadman Notification setup, contiune to setup a new one
                let content = UNMutableNotificationContent()
                content.title = AppConfig.shared.localized.notificationDataOutdatedTitle!
                content.body = AppConfig.shared.localized.notificationDataOutdatedBody!
                content.sound = .default

                let trigger = UNTimeIntervalNotificationTrigger(
                    timeInterval: 24 * 60 * 60,
                    repeats: false
                )

                let request = UNNotificationRequest(
                    identifier: deadmanNotificationIdentifier,
                    content: content,
                    trigger: trigger
                )
                
                notificationCenter.add(request) { error in
                    DispatchQueue.main.async {
                        if let error = error {
                            Log.log("BGNotification: Error showing user notification \(error)")
                        }
                    }
                }
            }
        })
    }
    
    private static let deadmanNotificationIdentifier = (Bundle.main.bundleIdentifier ?? "") + ".notifications.deadman"
    
    func showExposureNotif() {
        Analytics.logEvent(ERA.app_exposure_detected.rawValue, parameters: nil)
        showExposureNotification(result: AppConfig.shared.localized.notificationExposedBody!)
    }
    
    func showExposurePausedStateNotif() {
        
        showExposurePausedStateNotification()
    }

}

extension BackgroundService {
    
    static func getNotificationIdentifier() -> String {
        guard let bundleID = Bundle.main.bundleIdentifier else {
            Log.log("BGNotification: Could not access bundle identifier")
            return "sk.it.erusko.notifications.exposures"
        }
        return bundleID + ".notifications.exposures"
    }
    
    static func getExposurePausedNotificationIdentifier() -> String {
        guard let bundleID = Bundle.main.bundleIdentifier else {
            return "sk.it.erusko.notifications.exposuresPaused"
        }
        return bundleID + ".notifications.exposuresPaused"
    }
}

private extension BackgroundService {
    
    func performExposurePausedStateTask(_ task: BGTask?) -> Progress {
        let progress = Progress()
        
        log("BG: paused state check -> performed now \(exposureService)")

        if exposureService.status != .active {
            showExposurePausedStateNotif()
        }
        task?.setTaskCompleted(success: true)
        return progress
    }

    func performTask(_ task: BGTask?) -> Progress {
        
        log("BG: exposure check -> performing now")

        Analytics.logEvent(ERA.app_exposure_batch_download_finished.rawValue, parameters: nil)

        isRunning = true

        // Notify the user if bluetooth is off
        showBluetoothOffUserNotificationIfNeeded()

        func reportFailure(_ error: Error) {
            
            #if !APPSTORE || DEBUG
            log("===============================================")
            log("EXP: report Failure error \(error)")
            log("===============================================")
            #endif
            task?.setTaskCompleted(success: false)
            isRunning = false
            Log.log("BGTask: failed to detect exposures \(error)")
        }

        guard exposureService.authorizationStatus == .authorized else {
            reportFailure(ExposureError.restrictedAccess)
            return Progress()
        }
        
        // Perform the exposure detection
        return reporter.downloadKeys(lastProcessedFileName: AppStorage.lastProcessedFileName) { result in
            Log.log("BGTask: did download keys \(result)")

            switch result {
            case .success(let keys):
                Self.scheduleDeadmanNotification(override: true)
                guard !keys.URLs.isEmpty else {
//                    AppStorage.lastProcessedDate = Date()

                    #if !APPSTORE || DEBUG
                    log("===============================================")
                    log("EXP: no new keys")
                    log("===============================================")
                    #endif
                    self.isRunning = false

                    task?.setTaskCompleted(success: true)
                    return
                }

                self.exposureService.detectExposures(
                    configuration: AppConfig.shared.exposuresData,
                    URLs: keys.URLs
                ) { result in
                    switch result {
                    case .success(var exposures):
                        exposures.sort { $0.date < $1.date }
//                        AppStorage.lastProcessedDate = Date()

                        self.handleExposures(exposures, lastProcessedFileName: keys.lastProcessedFileName)
                        self.isRunning = false

                        task?.setTaskCompleted(success: true)

                        Analytics.logEvent(ERA.app_exposure_batch_download_started.rawValue, parameters: nil)
                    case .failure(let error):
                        trace(text: error.localizedDescription)
                        reportFailure(error)
                    }
                }
            case .failure(let error):
                trace(text: error.localizedDescription)
                reportFailure(error)
            }
        }
    }

    func handleExposures(_ exposures: [Exposure], lastProcessedFileName: String?) {
        if let fileName = lastProcessedFileName {
            AppStorage.lastProcessedFileName = fileName
        }

        guard !exposures.isEmpty else {
            log("EXP: no exposures, skip!")

            #if !APPSTORE || DEBUG
            log("===============================================")
            log("EXP: No exposures detected, device is clear.")
            log("===============================================")
            #endif
            return
        }

        exposures.forEach { AppStorage.exposures.append($0.date) }
        
        DispatchQueue.main.async {
            AppDelegate.shared.mainRouter?.refreshHome()
        }
        
        showExposureNotif()
    }

    func showExposureNotification(result: String) {
        trace(text: "show exposure notif")

        let content = UNMutableNotificationContent()
        content.title = AppConfig.shared.localized.notificationExposedTitle!
        content.body = result
        content.sound = .default
        let request = UNNotificationRequest(identifier: Self.getNotificationIdentifier(),
                                            content: content,
                                            trigger: nil)
        UNUserNotificationCenter.current().add(request) { error in
            DispatchQueue.main.async {
                if let error = error {
                    Log.log("BGNotification: error showing error user notification \(error)")
                }
            }
        }

    }
    
    func showExposurePausedStateNotification() {
        trace(text: "show paused notif")
        
        let content = UNMutableNotificationContent()
        content.title = AppConfig.shared.localized.notificationScanningPausedTitle!
        content.body = AppConfig.shared.localized.notificationScanningPausedBody!
        content.sound = .default
        let request = UNNotificationRequest(identifier: Self.getExposurePausedNotificationIdentifier(),
                                            content: content,
                                            trigger: nil)
        UNUserNotificationCenter.current().add(request) { error in
            DispatchQueue.main.async {
                if let error = error {
                    Log.log("BGNotification: error showing error user notification \(error)")
                }
            }
        }

    }

}

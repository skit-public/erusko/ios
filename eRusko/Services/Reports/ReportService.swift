//
//  ReportService.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import Foundation
import ExposureNotification
import FirebaseAuth
import Alamofire
import Zip
import CryptoKit

protocol ReportServicing: AnyObject {

    func updateConfiguration(serverUrl: URL?, exportUrl: URL?, healthAuthority: String?)

    func calculateHmacKey(keys: [ExposureDiagnosisKey], secret: Data) throws -> String

    typealias UploadKeysCallback = (Result<Bool, Error>) -> Void

    var isUploading: Bool { get }
    func uploadKeys(keys: [ExposureDiagnosisKey], verificationPayload: String, hmacSecret: Data, traveler: Bool, callback: @escaping UploadKeysCallback)

    typealias DownloadKeysCallback = (Result<ReportKeys, Error>) -> Void

    var isDownloading: Bool { get }
    func downloadKeys(lastProcessedFileName: String?, callback: @escaping DownloadKeysCallback) -> Progress

}

final class ReportService: ReportServicing {

    private var healthAuthority: String?

    private var uploadURL: URL?

    private var downloadBaseURL: URL?
    private var downloadDestinationURL: URL {
        let directoryURLs = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        guard let documentsURL = directoryURLs.first else {
            fatalError("No documents directory!")
        }
        return documentsURL
    }

    init(serverUrl: URL?, exportUrl: URL?, healthAuthority: String?) {
        log("ReportService init: \(String(describing: serverUrl?.absoluteString)) | \(String(describing: exportUrl?.absoluteString))")
        self.healthAuthority = healthAuthority
        self.uploadURL = serverUrl
        self.downloadBaseURL = exportUrl
    }

    func updateConfiguration(serverUrl: URL?, exportUrl: URL?, healthAuthority: String?) {
        log("ReportService update: \(String(describing: serverUrl?.absoluteString)) | \(String(describing: exportUrl?.absoluteString))")
        self.healthAuthority = healthAuthority
        self.uploadURL = serverUrl
        self.downloadBaseURL = exportUrl
    }

    func calculateHmacKey(keys: [ExposureDiagnosisKey], secret: Data) throws -> String {
        // Sort by the key.
        let sortedKeys = keys.sorted { lhs, rhs -> Bool in
            lhs.keyData.base64EncodedString() < rhs.keyData.base64EncodedString()
        }

        // Build the cleartext.
        let perKeyClearText: [String] = sortedKeys.map { key in
            [key.keyData.base64EncodedString(),
             String(key.rollingStartNumber),
             String(key.rollingPeriod),
             String(key.transmissionRiskLevel)].joined(separator: ".")
        }
        let clearText = perKeyClearText.joined(separator: ",")

        guard let clearData = clearText.data(using: .utf8) else {
            throw ReportError.stringEncodingFailure
        }

        let hmacKey = SymmetricKey(data: secret)
        let authenticationCode = HMAC<SHA256>.authenticationCode(for: clearData, using: hmacKey)
        return authenticationCode.withUnsafeBytes { bytes in
            Data(bytes)
        }.base64EncodedString()
    }

    private(set) var isUploading: Bool = false

    func uploadKeys(keys: [ExposureDiagnosisKey], verificationPayload: String, hmacSecret: Data, traveler: Bool, callback: @escaping UploadKeysCallback) {
        guard !isUploading else {
            callback(.failure(ReportError.alreadyRunning))
            return
        }
        
        guard
            let healthAuthority = self.healthAuthority,
            let uploadURL = self.uploadURL
        else {
            log("ReportService Upload error: No configuration data")
            callback(.failure(ReportError.unknown))
            return
        }
        isUploading = true

        func reportFailure(_ error: Error) {
            log("ReportService Upload error: \(error)")
            DispatchQueue.main.async {
                self.isUploading = false
                callback(.failure(error))
            }
            return
        }

        func reportSuccess(_ result: ReportResult) {
            log("ReportService Upload done")
            DispatchQueue.main.async {
                self.isUploading = false
                if result.revisionToken != nil {
                    KeychainService.revisionToken = result.revisionToken
                }
//                AppStorage.lastUploadDate = Date()
                callback(.success(true))
            }
        }

        let randomInt = Int.random(in: 0...100)
        let randomBase64 = Data.random(count: randomInt + 100).base64EncodedString()
        
        let report = Report(
            temporaryExposureKeys: keys,
            healthAuthority: healthAuthority,
            verificationPayload: verificationPayload,
            hmacKey: hmacSecret.base64EncodedString(),
            symptomOnsetInterval: nil,
            traveler: traveler,
            revisionToken: KeychainService.revisionToken,
            padding: randomBase64
        )

        
        AF.request(uploadURL, method: .post, parameters: report, encoder: JSONParameterEncoder.default)
            .validate(statusCode: 200..<300)
            .responseDecodable(of: ReportResult.self) { response in
                #if DEBUG
                log("Response upload")
                debugPrint(response)
                #endif

                switch response.result {
                case .success(let result):
                    reportSuccess(result)
                case .failure(let error):
                    if let erError = ERError.fromData(data: response.data) {
                        reportFailure(erError)
                    } else {
                        reportFailure(error)
                    }
                }
            }
    }

    private(set) var isDownloading: Bool = false

    func downloadKeys(lastProcessedFileName: String?, callback: @escaping DownloadKeysCallback) -> Progress {
        let progress = Progress()

        guard !isDownloading else {
            callback(.failure(ReportError.alreadyRunning))
            return progress
        }
        guard let downloadBaseURL = self.downloadBaseURL else {
            log("ReportService Download error: No configuration data")
            callback(.failure(ReportError.unknown))
            return progress
        }
        isDownloading = true

        func reportFailure(_ error: Error) {
            log("ReportService Download error: \(error)")
            isDownloading = false
            callback(.failure(error))
        }

        func reportSuccess(_ reports: [URL], lastProcessFileName: String?) {
            log("ReportService Download done")
            isDownloading = false
            callback(.success(ReportKeys(URLs: reports, lastProcessedFileName: lastProcessFileName)))
        }

        let destinationURL = self.downloadDestinationURL
        let destination: DownloadRequest.Destination = { temporaryURL, response in
            let url = destinationURL.appendingPathComponent(response.suggestedFilename ?? "Exposures.zip")
            return (url, [.removePreviousFile, .createIntermediateDirectories])
        }
        var downloads: [DownloadRequest] = []

        AF.request(downloadBaseURL, method: .get)
            .validate(statusCode: 200..<300)
            .responseString { response in
                #if DEBUG
                log("Response index")
                debugPrint(response)
                #endif
                
                let dispatchGroup = DispatchGroup()
                var localURLResults: [Result<[URL], Error>] = []
                var lastRemoteURL: URL?

                switch response.result {
                case let .success(result):
                    let parsedURLs = ERParser(xml: result, downloadBaseURL: downloadBaseURL).parseXML()
                    var remoteURLs: [ReportFile] = []
                    remoteURLs = parsedURLs.filter {
                        return !$0.key.lastPathComponent.contains("index.txt") && !(AppStorage.eTagsList.contains($0.eTag.replacingOccurrences(of: "\"", with: "")))
                    }

                    lastRemoteURL = remoteURLs.last?.key

                    for remoteURL in remoteURLs {
                        dispatchGroup.enter()

                        try? FileManager.default.removeItem(at: destinationURL.appendingPathComponent(remoteURL.key.lastPathComponent).deletingPathExtension())

                        let download = AF.download(remoteURL.key, to: destination)
                            .validate(statusCode: 200..<300)
                            .response { response in
                                #if DEBUG
                                log("Response file")
                                debugPrint(response)
                                #endif

                                switch response.result {
                                case .success(let downloadedURL):
                                    guard let downloadedURL = downloadedURL else {
                                        localURLResults.append(.failure(ReportError.noFile))
                                        break
                                    }
                                    do {
                                        AppStorage.eTagsList.append(remoteURL.eTag.replacingOccurrences(of: "\"", with: ""))
                                        let unzipDirectory = try Zip.quickUnzipFile(downloadedURL)
                                        let fileURLs = try FileManager.default.contentsOfDirectory(
                                            at: unzipDirectory,
                                            includingPropertiesForKeys: [], options: [.skipsHiddenFiles]
                                        )
                                        let uniqueName = UUID().uuidString
                                        let changedNames: [URL] = try fileURLs.map {
                                            var newURL = $0
                                            newURL.deleteLastPathComponent()
                                            newURL.deleteLastPathComponent()
                                            newURL.appendPathComponent("exposures")
                                            try FileManager.default.createDirectory(at: newURL, withIntermediateDirectories: true, attributes: nil)
                                            newURL.appendPathComponent(uniqueName)
                                            newURL.appendPathExtension($0.pathExtension)
                                            try FileManager.default.moveItem(at: $0, to: newURL)
                                            return newURL
                                        }
                                        try FileManager.default.removeItem(at: downloadedURL)
                                        try FileManager.default.removeItem(at: unzipDirectory)
                                        localURLResults.append(.success(changedNames))
                                    } catch {
                                        trace(text: error.localizedDescription)
                                        localURLResults.append(.failure(error))
                                    }
                                case .failure(let error):
                                    trace(text: error.localizedDescription)
                                    localURLResults.append(.failure(error))
                                }

                                if progress.isCancelled {
                                    downloads.forEach { $0.cancel() }
                                }
                                dispatchGroup.leave()
                            }
                        progress.addChild(download.downloadProgress, withPendingUnitCount: 1)
                        downloads.append(download)
                    }
                case let .failure(error):
                    DispatchQueue.main.async {
                        switch error {
                        case .responseSerializationFailed(reason: .inputDataNilOrZeroLength):
                            reportSuccess([], lastProcessFileName: nil)
                        default:
                            trace(text: error.localizedDescription)
                            reportFailure(error)
                        }
                    }
                }

                dispatchGroup.notify(queue: .main) {
                    if progress.isCancelled {
                        reportFailure(ReportError.cancelled)
                        return
                    }

                    var localURLs: [URL] = []
                    for result in localURLResults {
                        switch result {
                        case let .success(URLs):
                            localURLs.append(contentsOf: URLs)
                        case let .failure(error):
                            trace(text: error.localizedDescription)
                            reportFailure(error)
                            return
                        }
                    }

                    reportSuccess(localURLs, lastProcessFileName: lastRemoteURL?.lastPathComponent)
                }
            }

        return progress
    }

}

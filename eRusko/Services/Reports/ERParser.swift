//
//  ERParser.swift
//  eRusko
//
//  Created by Slovensko IT on 23/10/2020.
//

import Foundation

struct ReportFile {
    var key: URL = URL(string: "http://google.sk")!
    var generation: String = ""
    var metaGeneration: String = ""
    var lastModified: Date = Date()
    var eTag: String = ""
    var size: String = ""
}

class ERParser: NSObject {
    var parser: XMLParser
    
    let downloadBaseURL: URL
    
    var files: [ReportFile] = []
    var elementName: String = String()
    var key = String()
    var generation = String()
    var metaGeneration = String()
    var lastModified = String()
    var eTag = String()
    var size = String()

    init(xml: String, downloadBaseURL: URL) {
        parser = XMLParser(data: xml.data(using: String.Encoding.utf8)!)
        self.downloadBaseURL = downloadBaseURL
        super.init()
        parser.delegate = self
    }

    func parseXML() -> [ReportFile] {
        parser.parse()
        return files
    }

}

extension ERParser: XMLParserDelegate {

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {

        if elementName == "Contents" {
            key = String()
            generation = String()
            metaGeneration = String()
            lastModified = String()
            eTag = String()
            size = String()
        }

        self.elementName = elementName
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        if (!data.isEmpty) {
            if self.elementName == "Key" {
                key += data
            } else if self.elementName == "Generation" {
                generation += data
            } else if self.elementName == "MetaGeneration" {
                metaGeneration += data
            } else if self.elementName == "LastModified" {
                lastModified += data
            } else if self.elementName == "ETag" {
                eTag += data
            } else if self.elementName == "Size" {
                size += data
            }
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "Contents" {
            var file = ReportFile()
            file.key = self.downloadBaseURL.appendingPathComponent(String(key))
            file.generation = generation
            file.metaGeneration = metaGeneration
            file.eTag = eTag
            file.size = size
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            if let date = dateFormatter.date(from: lastModified) {
                file.lastModified =  date
            }
            files.append(file)
        }
    }
}


//
//  Logger.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import Foundation

func log(_ text: String) {
    #if !APPSTORE
    DispatchQueue.main.async {
        Log.log(text)
        FileLogger.shared.writeLog(text)
    }
    #elseif DEBUG
    print(text)
    #endif
}

func trace(text: String? = nil, file: StaticString = #file, function: StaticString = #function, line: UInt = #line) {
    let file = URL(fileURLWithPath: String(describing: file)).deletingPathExtension().lastPathComponent
    var function = String(describing: function)
    function.removeSubrange(function.firstIndex(of: "(")! ... function.lastIndex(of: ")")!)
    guard let text = text else {
        log("trace: \(file) -> \(function) -> \(line)")
        return
    }
    log("trace: '\(text)' \(file) -> \(function) -> \(line)")
}

protocol LogDelegate: AnyObject {
    func didLog(_ text: String)
}

struct Log {
    static weak var delegate: LogDelegate?

    static func log(_ text: String) {
        delegate?.didLog(text)
        print(text)
    }
}

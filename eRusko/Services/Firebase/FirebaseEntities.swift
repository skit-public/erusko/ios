//
//  Localization.swift
//  eRusko
//
//  Created by Slovensko IT on 16/10/2020.
//

import Foundation
import ExposureNotification

struct ExposuresDataFBO: Codable {
    
    var diagnosisKeysPeriodicFrameHours: Double = 6
    let keyImportDataOutdatedHours: Int?
    let diagnosisKeysDataMappingLimitDays: Int?
    let daysSinceOnsetToInfectiousness: [Int]?
    let reportTypeWhenMissing: Int?
    let reportTypeWeights: [Double]?
    var attenuationBucketThresholdDb: [Int] = [55, 63, 75]
    var attenuationBucketWeights: [Double] = [1.5, 1.0, 0.16, 0.0]
    let infectiousnessWeights: [Double]?
    let minimumWindowScore: Double?
    var selfCheckerPeriodHours: Double = 4
    
    enum CodingKeys: String, CodingKey {
        case diagnosisKeysPeriodicFrameHours
        case keyImportDataOutdatedHours
        case diagnosisKeysDataMappingLimitDays
        case daysSinceOnsetToInfectiousness
        case reportTypeWhenMissing
        case reportTypeWeights
        case attenuationBucketThresholdDb
        case attenuationBucketWeights
        case infectiousnessWeights
        case minimumWindowScore
        case selfCheckerPeriodHours
    }
    
//    @available (iOS 13.7, *)
    var configuration: ENExposureConfiguration {
        let configuration = ENExposureConfiguration()
        
        configuration.attenuationDurationThresholds = attenuationBucketThresholdDb.map { NSNumber.init(value: $0)}
        if let minimumRiskScoreFullRange = minimumWindowScore {
            configuration.minimumRiskScoreFullRange = minimumRiskScoreFullRange
        }
        
        if #available(iOS 13.7, *) {

            configuration.immediateDurationWeight = attenuationBucketWeights[0] * 100
            configuration.nearDurationWeight = attenuationBucketWeights[1] * 100
            configuration.mediumDurationWeight = attenuationBucketWeights[2] * 100
            configuration.otherDurationWeight = attenuationBucketWeights[3] * 100
            if let infectiousnessForDaysSinceOnsetOfSymptoms = daysSinceOnsetToInfectiousness {
                var finalDict: [NSNumber: NSNumber] = [:]
                var y: Int = 0
                for i in -14...14 {
                    finalDict[NSNumber.init(value: i)] = NSNumber.init(value: infectiousnessForDaysSinceOnsetOfSymptoms[y])
                    y += 1
                }
                configuration.infectiousnessForDaysSinceOnsetOfSymptoms = finalDict
            }
            if let infectiousnessStandardWeight = infectiousnessWeights?[1] { configuration.infectiousnessStandardWeight = infectiousnessStandardWeight * 100 }
            if let infectiousnessHighWeight = infectiousnessWeights?[2] { configuration.infectiousnessHighWeight = infectiousnessHighWeight * 100 }
            if let reportTypeConfirmedTestWeight = reportTypeWeights?[0] { configuration.reportTypeConfirmedTestWeight = reportTypeConfirmedTestWeight * 100 }
            if let reportTypeConfirmedClinicalDiagnosisWeight = reportTypeWeights?[1] { configuration.reportTypeConfirmedClinicalDiagnosisWeight = reportTypeConfirmedClinicalDiagnosisWeight * 100 }
            if let reportTypeSelfReportedWeight = reportTypeWeights?[2] { configuration.reportTypeSelfReportedWeight = reportTypeSelfReportedWeight * 100 }
            if let reportTypeRecursiveWeight = reportTypeWeights?[3] { configuration.reportTypeRecursiveWeight = reportTypeRecursiveWeight * 100 }
            if let report = reportTypeWhenMissing, let reportTypeNoneMap = ENDiagnosisReportType(rawValue: UInt32(report)) { configuration.reportTypeNoneMap = reportTypeNoneMap }
        } else {
            // Fallback on earlier versions
        }

        return configuration
    }
}

extension ExposuresDataFBO {
    
    static var defaultValue: ExposuresDataFBO {
        return ExposuresDataFBO(diagnosisKeysPeriodicFrameHours: 6,
                                keyImportDataOutdatedHours: 24,
                                diagnosisKeysDataMappingLimitDays: 7,
                                daysSinceOnsetToInfectiousness: [0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                                reportTypeWhenMissing: 1,
                                reportTypeWeights: [1,1,1,1],
                                attenuationBucketThresholdDb: [55,63,75],
                                attenuationBucketWeights: [1.5,1,0.16,0],
                                infectiousnessWeights: [1,1,1],
                                minimumWindowScore: 900,
                                selfCheckerPeriodHours: 4)
    }
}

struct ExposuresServerFBO: Codable {
    
    var keyServerUrl: URL
    var verificationServerUrl: URL
    var healthAuthorityID: String
    var keyExportUrl: URL
    var verificationServerApiKey: String
    
    enum CodingKeys: String, CodingKey {
        case keyServerUrl
        case verificationServerUrl
        case healthAuthorityID
        case keyExportUrl
        case verificationServerApiKey
    }
}

struct LocalizationFBO: Codable {
    
    let locale: String?
    let texts: Localized?
    
    enum CodingKeys: String, CodingKey {
        case locale
        case texts
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        locale = try values.decodeIfPresent(String.self, forKey: .locale)
        texts = try values.decodeIfPresent(Localized.self, forKey: .texts)
    }
    
    init(locale: String, texts: Localized) {
        self.locale = locale
        self.texts = texts
    }
}

extension LocalizationFBO {
    
    static var defaultValue: [LocalizationFBO] {
        return [LocalizationFBO(locale: "SK",
                                texts: Localized())]
                                    
    }
}

struct VersionControlFBO: Codable {
    
    var latestVersion: Version?
    var mandatoryVersion: Version?
    
    enum CodingKeys: String, CodingKey {
        case latestVersion
        case mandatoryVersion
    }
    
    init(from decoder:Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        latestVersion = Version("1.0.0")
        mandatoryVersion = Version("1.0.0")
        if let latestVersionT  = try values.decodeIfPresent(String.self, forKey: .latestVersion) {
            latestVersion = Version(latestVersionT)
        }
        if let mandatoryVersionT = try values.decodeIfPresent(String.self, forKey: .mandatoryVersion) {
            mandatoryVersion = Version(mandatoryVersionT)
        }
    }
}

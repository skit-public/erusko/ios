//
//  FirebaseService.swift
//  eRusko
//
//  Created by Slovensko IT on 22/10/2020.
//

import Foundation
import FirebaseRemoteConfig



struct FirebaseService {
    
    // MARK: Properties
    
    static var needFetch: Bool {
        guard let fetchDate = RemoteConfig.remoteConfig().lastFetchTime else {
            return true
        }
        let thresholdDate = Date(timeIntervalSince1970: 1)
        let returnValue: Bool = (fetchDate < thresholdDate)
        return returnValue
    }
    
    static func fetch(completion: @escaping (_ error: Error?) -> Void) {
        #if DEBUG
        let fetchDuration: TimeInterval = 0
        #else
        let fetchDuration: TimeInterval = background ? 1_800 : 3_600
        #endif

        RemoteConfig.remoteConfig().fetch(withExpirationDuration: fetchDuration) { _, error in
            if let error = error {
                log("AppDelegate: Got an error fetching remote values \(error)")
                completion(error)
                return
            }

            RemoteConfig.remoteConfig().activate { (changed, error) in
                
                log("AppDelegate: Retrieved values from the Firebase Remote Config!")
                
                AppConfig.shared.update()

                AppDelegate.dependency.reporter.updateConfiguration(
                    serverUrl: AppConfig.shared.exposuresServer?.keyServerUrl,
                    exportUrl: AppConfig.shared.exposuresServer?.keyExportUrl,
                    healthAuthority: AppConfig.shared.exposuresServer?.healthAuthorityID
                )

                AppDelegate.dependency.verification.updateConfiguration(
                    url: AppConfig.shared.exposuresServer?.verificationServerUrl,
                    deviceKey: AppConfig.shared.exposuresServer?.verificationServerApiKey
                )
                AppStorage.serverConfiguration = AppConfig.shared.exposuresServer

                completion(error)
            }
        }
    }
}

//
//  VerificationEntities.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import Foundation

enum VerificationError: Error {
    case noData
    case responseError(String, String)
    

}

enum VerificationTestType: String, Codable {
    case likely
    case confirmed
    case negative
}



struct VerificationCodeRequest: Encodable {

    let testType: VerificationTestType

    /// YYYY-MM-DD
    let symptomDate: String

    private enum CodingKeys: String, CodingKey {
        case testType = "testtype"
        case symptomDate
    }

}

struct VerificationCode: Decodable {

    let code: String?
    let expiresAt: Date?
    let error: String?

}

struct VerificationTokenRequest: Encodable {

    let code: String

}

struct VerificationToken: Decodable {

    let testType: VerificationTestType?
    let symptomDate: String?
    let token: String?
    let error: String?

    private enum CodingKeys: String, CodingKey {
        case testType = "testtype"
        case symptomDate
        case token = "token"
        case error
    }

}

struct VerificationCertificateRequest: Encodable {

    let token: String
    let hmacKey: String

    private enum CodingKeys: String, CodingKey {
        case token = "token"
        case hmacKey = "ekeyhmac"
    }

}

struct VerificationCertificate: Decodable {

    let certificate: String?
    let error: String?
    
    private enum CodingKeys: String, CodingKey {
        case certificate
        case error
    }
}

struct ERError: Error, Decodable {
    
    let error: String?
    var errorCode: String = ""
    
    private enum CodingKeys: String, CodingKey {
        case error
        case errorCode
        case error_code
    }
    
    public init(from decoder:Decoder) throws {

        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        error  = try values.decodeIfPresent(String.self, forKey: .error)
        var errorCodeF = try values.decodeIfPresent(String.self, forKey: .errorCode)
        if errorCodeF == nil {
            errorCodeF = try values.decodeIfPresent(String.self, forKey: .error_code)
        }
        errorCode = errorCodeF ?? "UNPARSABLE_REQUEST"
    }
    
    public static func fromData(data: Data?) -> ERError? {
        
        if let data = data {
            let decoder = JSONDecoder()
            let erError = try? decoder.decode(ERError.self, from: data)
            if let erError = erError {
                return erError
            }
        }
        return nil
    }
    
    func getMessageBasedOnCode() -> (String, String) {

        switch self.errorCode.uppercased() {
        case "CODE_INVALID":
            return (AppConfig.shared.localized.submitKeysErrorCodeInvalidTitle!, AppConfig.shared.localized.submitKeysErrorCodeInvalidBody!)
        case "CODE_EXPIRED":
            return (AppConfig.shared.localized.submitKeysErrorCodeExpiredTitle!, AppConfig.shared.localized.submitKeysErrorCodeExpiredBody!)
        case "CODE_NOT_FOUND":
            return (AppConfig.shared.localized.submitKeysErrorCodeInvalidTitle!, AppConfig.shared.localized.submitKeysErrorCodeInvalidTitle!)
        case "UNPARSABLE_REQUEST":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "INVALID_TEST_TYPE":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "MISSING_DATE":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "UNSUPPORTED_TEST_TYPE":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "TOKEN_INVALID":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "TOKEN_EXPIRED":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "HMAC_INVALID":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "UNKNOWN_HEALTH_AUTHORITY_ID":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "UNABLE_TO_LOAD_HEALTH_AUTHORITY":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "HEALTH_AUTHORITY_MISSING_REGION_CONFIG":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "HEALTH_AUTHORITY_VERIFICATION_CERTIFICATE_INVALID":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "BAD_REQUEST":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "INTERNAL_ERROR":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "MISSING_REVISION_TOKEN":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "KEY_ALREADY_REVISED":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "INVALID_REPORT_TYPE_TRANSITION":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        case "PARTIAL_FAILURE":
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        default:
            return (AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!)
        }
    }
}

//
//  VerificationService.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import Foundation
import Alamofire

protocol VerificationServicing: AnyObject {

    func updateConfiguration(url: URL?, deviceKey: String?)

    typealias VerifyCallback = (Result<String, Error>) -> Void
    func verify(with code: String, callback: @escaping VerifyCallback)

    typealias CertificateCallback = (Result<String, Error>) -> Void
    func requestCertificate(token: String, hmacKey: String, callback: @escaping CertificateCallback)

}

final class VerificationService: VerificationServicing {

    private var serverURL: URL?
    private let headerApiKey = "X-API-Key"

    private var deviceKey: String?

    init(url: URL?, deviceKey: String?) {
        self.serverURL = url
        self.deviceKey = deviceKey
    }

    func updateConfiguration(url: URL?, deviceKey: String?) {
        self.serverURL = url
        self.deviceKey = deviceKey
    }


    func verify(with code: String, callback: @escaping VerifyCallback) {
        
        guard let deviceKey = deviceKey else {
            log("VerificationService error: No configuration data")
            callback(.failure(VerificationError.noData))
            return
        }
        
        var headers = HTTPHeaders()
        headers.add(HTTPHeader(name: headerApiKey, value: deviceKey))

        let request = VerificationTokenRequest(code: code)
        // swiftlint:disable:next force_unwrapping
        AF.request(URL(string: "api/verify", relativeTo: serverURL)!,
                   method: .post,
                   parameters: request,
                   encoder: JSONParameterEncoder.default,
                   headers: headers)
            .validate(statusCode: 200..<300)
            .responseDecodable(of: VerificationToken.self) { response in
                #if DEBUG
                debugPrint(response)
                #endif

                switch response.result {
                case .success(let result):
                    if let token = result.token {
                        callback(.success(token))
                    } else {
                        callback(.failure(VerificationError.noData))
                    }
                case .failure(let error):
                    
                    if let erError = ERError.fromData(data: response.data) {
                        callback(.failure(erError))
                    }
                    callback(.failure(error))
                }
            }
    }

    func requestCertificate(token: String, hmacKey: String, callback: @escaping CertificateCallback) {
        
        guard let deviceKey = deviceKey else {
            log("VerificationService error: No configuration data")
            callback(.failure(VerificationError.noData))
            return
        }
        
        var headers = HTTPHeaders()
        headers.add(HTTPHeader(name: headerApiKey, value: deviceKey))

        let request = VerificationCertificateRequest(token: token, hmacKey: hmacKey)
        // swiftlint:disable:next force_unwrapping
        AF.request(URL(string: "api/certificate", relativeTo: serverURL)!,
                   method: .post,
                   parameters: request,
                   encoder: JSONParameterEncoder.default,
                   headers: headers)
            .validate(statusCode: 200..<300)
            .responseDecodable(of: VerificationCertificate.self) { response in
                #if DEBUG
                debugPrint(response)
                #endif

                switch response.result {
                case .success(let result):
                    if let certificate = result.certificate {
                        callback(.success(certificate))
                    } else {
                        callback(.failure(VerificationError.noData))
                    }
                case .failure(let error):
                    
                    if let erError = ERError.fromData(data: response.data) {
                        callback(.failure(erError))
                    }
                    callback(.failure(error))
                }
            }
    }

}

//
//  ExposureService.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import Foundation
import ExposureNotification

protocol ExposureServicing: AnyObject {

    var readyToUse: Error? { get }

    // Activation
    typealias Callback = (Error?) -> Void

    var isActive: Bool { get }
    var isEnabled: Bool { get }

    var status: ENStatus { get }
    var authorizationStatus: ENAuthorizationStatus { get }

    typealias ActivationCallback = (ExposureError?) -> Void
    func activate(callback: ActivationCallback?)
    func deactivate(callback: Callback?)

    // Keys
    typealias KeysCallback = (_ result: Result<[ExposureDiagnosisKey], ExposureError>) -> Void
    func getDiagnosisKeys(callback: @escaping KeysCallback)
    func getTestDiagnosisKeys(callback: @escaping KeysCallback)

    // Traveler
    typealias TravelerCallback = (Result<Bool, Error>) -> Void

    @available(iOS 13.7, *)
    func getUserTraveled(callback: @escaping TravelerCallback)

    // Detection
    typealias DetectCallback = (Result<[Exposure], Error>) -> Void
    var detectingExposures: Bool { get }
    func detectExposures(configuration: ExposuresDataFBO, URLs: [URL], callback: @escaping DetectCallback)

    // Bluetooth
    var isBluetoothOn: Bool { get }

}

final class ExposureService: ExposureServicing {

    var readyToUse: Error?

    typealias Callback = (Error?) -> Void

    private var manager: ENManager

    var isActive: Bool {
        [ENStatus.active, .paused].contains(manager.exposureNotificationStatus)
    }

    var isEnabled: Bool {
        manager.exposureNotificationEnabled
    }

    var status: ENStatus {
        manager.exposureNotificationStatus
    }

    var authorizationStatus: ENAuthorizationStatus {
        ENManager.authorizationStatus
    }

    init() {
        manager = ENManager()
        manager.activate { [weak self] error in
            if let error = error {
                self?.readyToUse = error
            } else {
                self?.readyToUse = nil
            }
            NotificationCenter.default.post(name: Notification.Name("ENManagerStateChanged"), object: error)
        }
    }

    deinit {
        manager.invalidate()
    }

    func activate(callback: ActivationCallback?) {
        log("ExposureService: activating")
        guard !isEnabled, !isActive else {
            callback?(nil)
            return
        }

        let activationCallback: ENErrorHandler = { [weak self] error in
            guard let self = self else { return }
            if let error = error {
                if let code = ENError.Code(rawValue: (error as NSError).code) {
                    callback?(ExposureError.activationError(code))
                } else if self.manager.exposureNotificationStatus == .restricted {
                    callback?(ExposureError.restrictedAccess)
                } else {
                    callback?(ExposureError.error(error))
                }
                return
            }

            DispatchQueue.main.async {
                callback?(nil)
            }
        }

        switch manager.exposureNotificationStatus {
        case .active, .paused:
            callback?(nil)
        case .disabled, .unknown, .restricted, .unauthorized:
            // Restricted should be not "activatable" but on actual device it always shows as restricted before activation
            manager.setExposureNotificationEnabled(true, completionHandler: activationCallback)
        case .bluetoothOff:
            callback?(ExposureError.bluetoothOff)
        @unknown default:
            callback?(ExposureError.unknown)
        }
    }

    func deactivate(callback: Callback?) {
        log("ExposureService: deactivating")
        guard isEnabled else {
            callback?(nil)
            return
        }

        manager.setExposureNotificationEnabled(false) { error in
            guard error == nil else {
                callback?(error)
                return
            }
            callback?(nil)
        }
    }

    func getDiagnosisKeys(callback: @escaping KeysCallback) {
        manager.getDiagnosisKeys(completionHandler: keysCallback(callback))
    }

    func getTestDiagnosisKeys(callback: @escaping KeysCallback) {
        manager.getTestDiagnosisKeys(completionHandler: keysCallback(callback))
    }

    @available(iOS 13.7, *)
    func getUserTraveled(callback: @escaping TravelerCallback) {
        manager.getUserTraveled(completionHandler: { traveler, error in
            if let error = error {
                callback(.failure(error))
            } else {
                callback(.success(traveler))
            }
        })
    }

    private func keysCallback(_ callback: @escaping KeysCallback) -> ENGetDiagnosisKeysHandler {
        return { keys, error in
            if let error = error {
                callback(.failure(ExposureError.error(error)))
            } else if keys?.isEmpty == true {
                callback(.failure(ExposureError.noData))
            } else if let keys = keys {
                callback(.success(keys.map { ExposureDiagnosisKey(key: $0) }))
            }
        }
    }

    private(set) var detectingExposures = false

    func detectExposures(configuration: ExposuresDataFBO, URLs: [URL], callback: @escaping DetectCallback) {
        guard !detectingExposures else {
            callback(.failure(ExposureError.alreadyRunning))
            return
        }
        detectingExposures = true
        
        func finish(error: Error? = nil, exposures: [Exposure] = []) {
            if let error = error {
                trace(text: error.localizedDescription)
                callback(.failure(error))
            } else {
                callback(.success(exposures))
            }
            
            detectingExposures = false
            
            URLs.forEach {
                do {
                    try FileManager.default.removeItem(at: $0)
                } catch {
                    log("trying removing files but with error \(error)")
                }
            }
        }
        
        print("ExposureService detectExposures \(configuration.configuration)")
        self.manager.detectExposures(configuration: configuration.configuration, diagnosisKeyURLs: URLs) { summary, error in
            
            guard error == nil else {
                trace(text: error?.localizedDescription)
                finish(error: error)
                return
            }
            
            guard let summary = summary else {
                finish(error: ExposureError.noSummary)
                return
            }
            
            log("ExposureService summary \(summary)")
            
            if #available(iOS 13.7, *) {
                
                guard summary.daySummaries.count != 0 else {
                    finish(error: ExposureError.noDaySummaries)
                    return
                }
                
                var highestDaySummary = summary.daySummaries.max { (one, two) -> Bool in
                    one.date > two.date
                }
                if highestDaySummary == nil {
                    highestDaySummary = summary.daySummaries.first
                }
                if highestDaySummary!.date.isBiggerThen(AppStorage.lastExposureWarningDate) {
                    
                    log("ExposureService Summary meets requirements")
                    
                    finish(exposures: summary.daySummaries.map {
                        Exposure(
                            id: UUID(),
                            date: $0.date
                        )
                    })
                    AppStorage.lastExposureWarningDate = highestDaySummary!.date
                    
                } else {
                    finish(error: ExposureError.noNewExposures)
                }
                
            } else {
                finish(error: ExposureError.noData)
            }
        }
    }

    // MARK: - Bluetooth

    var isBluetoothOn: Bool {
        manager.exposureNotificationStatus != .bluetoothOff
    }

}

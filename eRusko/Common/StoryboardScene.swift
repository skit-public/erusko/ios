

import Foundation
import UIKit

// MARK: - Storyboard Scenes

internal enum StoryboardScene {
    internal enum Home: StoryboardType {
        internal static let storyboardName = "Home"
        internal static let initialScene = InitialSceneType<HomeViewController>(storyboard: Home.self)
    }
    
    internal enum Debug: StoryboardType {
        internal static let storyboardName = "Debug"
        internal static let tabBar = InitialSceneType<UINavigationController>(storyboard: Debug.self)
    }
    
    internal enum Error: StoryboardType {
        internal static let storyboardName = "Error"
        internal static let initialScene = InitialSceneType<ErrorViewController>(storyboard: Error.self)
    }
    
    internal enum ForceUpdate: StoryboardType {
        internal static let storyboardName = "ForceUpdate"
        internal static let forceUpdateVC = InitialSceneType<ForceUpdateViewController>(storyboard: ForceUpdate.self)
    }
    
    internal enum Help: StoryboardType {
        internal static let storyboardName = "Help"
        internal static let initialScene = InitialSceneType<HelpViewController>(storyboard: Help.self)
        internal static let help = SceneType<HelpViewController>(storyboard: Help.self, identifier: "Help")
    }
    
    internal enum Tac: StoryboardType {
        internal static let storyboardName = "Tac"
        internal static let initialScene = InitialSceneType<TacViewController>(storyboard: Tac.self)
    }
    
    internal enum About: StoryboardType {
        internal static let storyboardName = "About"
        internal static let initialScene = InitialSceneType<AboutViewController>(storyboard: About.self)
    }
    
    internal enum LaunchScreen: StoryboardType {
        internal static let storyboardName = "LaunchScreen"
        internal static let initialScene = InitialSceneType<UIKit.UIViewController>(storyboard: LaunchScreen.self)
    }
    
    internal enum Onboarding: StoryboardType {
        internal static let storyboardName = "Onboarding"
        internal static let initialScene = InitialSceneType<FirstActivationViewController>(storyboard: Onboarding.self)
        internal static let exposurePermissions = SceneType<ExposurePermissionViewController>(storyboard: Onboarding.self, identifier: "ExposurePermissionViewController")
        internal static let privacy = SceneType<PrivacyViewController>(storyboard: Onboarding.self, identifier: "PrivacyViewController")
    }

    internal enum Success: StoryboardType {
        internal static let storyboardName = "Success"
        internal static let initialScene = InitialSceneType<SuccessViewController>(storyboard: Success.self)
    }
    
    internal enum Risky: StoryboardType {
        internal static let storyboardName = "Risky"
        internal static let negativeScene = SceneType<RiskyViewControllerNegative>(storyboard: Risky.self, identifier: "RiskyViewControllerNegative")
        internal static let positiveScene = SceneType<RiskyViewControllerPositive>(storyboard: Risky.self, identifier: "RiskyViewControllerPositive")
    }
    
    internal enum RiskyHP: StoryboardType {
        internal static let storyboardName = "RiskyHP"
        internal static let initialScene = InitialSceneType<RiskyHPViewController>(storyboard: RiskyHP.self)
    }
    
    internal enum RiskyZZS: StoryboardType {
        internal static let storyboardName = "RiskyZZS"
        internal static let initialScene = InitialSceneType<RiskyZZSViewController>(storyboard: RiskyZZS.self)
    }
    
    internal enum RiskyRS: StoryboardType {
        internal static let storyboardName = "RiskyRS"
        internal static let initialScene = InitialSceneType<RiskyRSViewController>(storyboard: RiskyRS.self)
    }
    
    internal enum Intro: StoryboardType {
        internal static let storyboardName = "Intro"
        internal static let initialScene = InitialSceneType<IntroViewController>(storyboard: Intro.self)
    }
    internal enum SendReports: StoryboardType {
        internal static let storyboardName = "SendReports"        
        internal static let initialScene = InitialSceneType<SendReportsViewController>(storyboard: SendReports.self)
    }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
    static var storyboardName: String { get }
}

internal extension StoryboardType {
    static var storyboard: UIStoryboard {
        let name = self.storyboardName
        return UIStoryboard(name: name, bundle: BundleToken.bundle)
    }
}

internal struct SceneType<T: UIViewController> {
    internal let storyboard: StoryboardType.Type
    internal let identifier: String
    
    internal func instantiate() -> T {
        let identifier = self.identifier
        guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
            fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
        }
        return controller
    }
}

internal struct InitialSceneType<T: UIViewController> {
    internal let storyboard: StoryboardType.Type
    
    internal func instantiate() -> T {
        guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
            fatalError("ViewController is not of the expected class \(T.self).")
        }
        return controller
    }
}

// swiftlint:disable convenience_type
private final class BundleToken {
    static let bundle: Bundle = {
        Bundle(for: BundleToken.self)
    }()
}
// swiftlint:enable convenience_type

//
//  MainRouter.swift
//  eRusko
//
//  Created by Slovensko IT on 12/11/2020.
//

import UIKit
import FirebaseAnalytics

public final class MainRouter: NSObject {
    public let rootViewController: UINavigationController?

    public override init() {
        let vc = StoryboardScene.Intro.initialScene.instantiate()
        
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.navigationBar.isHidden = true
        
        self.rootViewController = navigationController
        
        super.init()
        navigationController.delegate = self
    }
    
    func updateInterface(animated: Bool = false) {

        var vc: UIViewController?

        if !isDeviceSupported() {
            vc = forceVC(.ForceUpdateUnsupported)
            
        } else if FirebaseService.needFetch {
            vc = forceVC(.ForceUpdateRetryConnection)
            
        } else if Version.currentOSVersion < AppConfig.shared.minSupportedOSVersion {
            vc = forceVC(.ForceUpdateOS)
            Analytics.logEvent(ERA.app_os_version_error.rawValue, parameters: nil)
            
        } else if
            let version = AppConfig.shared.versionControl?.mandatoryVersion,
            version > App.appVersion {
            vc = forceVC(.ForceUpdateMandatory)
            Analytics.logEvent(ERA.app_mandatory_update_error.rawValue, parameters: nil)
            
        } else if AppStorage.activated {
            vc = TemplateViewController.viewController(.Home)

        } else {
            vc = TemplateViewController.viewController(.FirstActivation)
        }
        
        guard let firstVC = vc else { return }

        rootViewController?.setViewControllers([firstVC], animated: animated)

    }
    
    func isDeviceSupported() -> Bool {
        #if targetEnvironment(simulator)
        return true
        #else
        let model = UIDevice.current.modelName
        if model.hasPrefix("iPhone") {
            // List of supported devices from https://support.apple.com/cs-cz/guide/iphone/iphe3fa5df43/13.0/ios/13.0
            let modelNumber = String(model[model.index(model.startIndex, offsetBy: 6)...])
            return Double(modelNumber.replacingOccurrences(of: ",", with: ".")) ?? 0 >= 8.0
        }
        return false
        #endif
    }

    func forceVC(_ state: TemplateControllerState) -> TemplateViewController {
        let vc = TemplateViewController.viewController(state)
        return vc
    }
    
    func refreshHome() {
        
        guard let tempScreen = rootViewController?.viewControllers.first as? TemplateViewController,
              let homeScreen = tempScreen.currentViewController as? HomeViewController else { return }
        homeScreen.checkExposuresAndShowBanner()
    }
}

extension MainRouter: UINavigationControllerDelegate {
   
    public func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        guard
            let fVC = fromVC as? OnboardingAnimatable, let _ = fVC.contentViewController as? OnboardingAnimatable,
            let tVC = toVC as? OnboardingAnimatable, let _ = tVC.contentViewController as? OnboardingAnimatable
        else {
            return nil
        }
        
        return OnboardingFrameAnimator(showPush: operation == .push)
    }

}

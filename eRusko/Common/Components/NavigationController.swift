//
//  NavigationController.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit

class NavigationController: UINavigationController {

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return .all
        } else {
            return .portrait
        }
    }

}

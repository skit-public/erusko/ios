//
//  Button.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit

class Button: UIButton {

    enum Style {
        case filled
        case clear
        case disabled
        case sendReports
        case riskyEncounters

        var backgroundColor: UIColor {
            switch self {
            case .filled:
                return UIColor.blackButtonBackground
            case .clear:
                return .clear
            case .disabled:
                return .clear
            case .sendReports:
                return UIColor.redButtonBackground
            case .riskyEncounters:
                return UIColor.blueButtonBackground
            }
        }

        var textColor: UIColor {
            switch self {
            case .filled:
                return UIColor.blackButtonText
            case .clear:
                return UIColor.black
            case .disabled:
                return .systemGray
            case .sendReports:
                return UIColor.redButtonText
            case .riskyEncounters:
                return UIColor.blueButtonText
            }
        }

        func setup(with button: UIButton, borderColor: UIColor?) {
            button.backgroundColor = backgroundColor

            button.layer.cornerRadius = 16
            button.layer.masksToBounds = true

            button.layer.borderWidth = [.filled, .sendReports, .riskyEncounters].contains(self) ? 0 : 1
            button.layer.borderColor = borderColor?.cgColor

            button.titleLabel?.textAlignment = .center
            button.setTitleColor(textColor, for: .normal)
            button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        }
    }

    var style: Style = .filled {
        didSet {
            if !isEnabled, style != .disabled {
                oldStyle = style
                style = .disabled
            }
            style.setup(with: self, borderColor: borderColor)
        }
    }

    private var oldStyle: Style = .filled

    override var isEnabled: Bool {
        get {
            super.isEnabled
        }
        set {
            super.isEnabled = newValue
            style = !newValue ? .disabled : oldStyle
        }
    }

    private var borderColor: UIColor? {
        switch style {
        case .filled, .sendReports, .riskyEncounters:
            return nil
        case .clear, .disabled:
            return UIColor.buttonBorder.resolvedColor(with: traitCollection).withAlphaComponent(0.12)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        style.setup(with: self, borderColor: borderColor)
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        layer.borderColor = borderColor?.cgColor
    }

}

final class RoundedButtonFilled: Button {

}

final class MainScanningButton: Button {

    override func awakeFromNib() {
        super.awakeFromNib()

        backgroundColor = UIColor.systemGray6
        setTitleColor(.systemBlue, for: .normal)
    }
}

final class RoundedButtonClear: Button {

    override func awakeFromNib() {
        style = .clear
        super.awakeFromNib()
    }
}

final class SendReportsButton: Button {

    override func awakeFromNib() {
        style = .sendReports
        super.awakeFromNib()
    }
}

final class RiskyEncountersButton: Button {

    override func awakeFromNib() {
        style = .riskyEncounters
        super.awakeFromNib()
    }
}

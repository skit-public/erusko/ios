//
//  TemplateViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 15/11/2020.
//

import UIKit

protocol NavigationBarMethodsDelegate {
    
    func navigationBarBackButtonMethod()
    func navigationBarRightButtonMethod()
}

extension NavigationBarMethodsDelegate {
    
    func navigationBarBackButtonMethod() {
        return
    }
    func navigationBarRightButtonMethod() {
        return
    }
}

enum TemplateControllerState: String {
    
    case FirstActivation
    case ExposurePermissions
    case Privacy
    case Home
    case RiskyEncountersNegative
    case RiskyEncountersPositive
    case SendReports
    case TaC
    case About
    case Help
    case ForceUpdateMandatory
    case ForceUpdateOS
    case ForceUpdateUnsupported
    case ForceUpdateRetryConnection
    case None
}

class TemplateViewController: BaseViewController {
    
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var embededContainerView: UIView!
    @IBOutlet weak var navigationBarHeightConstraint: NSLayoutConstraint!
    
    var currentViewController: BaseViewController? = nil
    
    var state: TemplateControllerState = .None
    
    var delegate: NavigationBarMethodsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure(basedOn: state)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationBar.navigationControl = self.navigationController

    }
    
    func configure(basedOn state: TemplateControllerState) {
        
        guard let vc = currentViewController else { return }

        self.addChild(vc)
        self.view.addSubview(vc.view)
    
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            vc.view.leadingAnchor.constraint(equalTo: embededContainerView.leadingAnchor),
            vc.view.trailingAnchor.constraint(equalTo: embededContainerView.trailingAnchor),
            vc.view.topAnchor.constraint(equalTo: embededContainerView.topAnchor),
            vc.view.bottomAnchor.constraint(equalTo: embededContainerView.bottomAnchor)
        ])
        vc.didMove(toParent: self)
        
        
        if let nvc = vc as? NavigationBarMethodsDelegate {
            self.delegate = nvc
        }
        
        navigationBar.manage(basedOn: state)
        
        navigationBar.backButtonCompletion = { [weak self] in
            self?.delegate?.navigationBarBackButtonMethod()
        }
        navigationBar.rightButtonCompletion = { [weak self] in
            self?.delegate?.navigationBarRightButtonMethod()
        }
        
        navigationBarHeightConstraint.constant = defaultNavBarHeight(basedOn: state)

        navigationBar.navigationBarHeightConstraint = navigationBarHeightConstraint
        navigationBar.deviceViewWidth = self.view.frame.size.width
        if let svc = vc as? ScrollAble {
            svc.scrollingControl.delegate = navigationBar
        }
        
        if let riskyPositive = vc as? RiskyViewControllerPositive {
            self.navigationBar.bannerTitleLabel.attributedText = AppConfig.shared.localized.dashboardBannerContact!.replacingOccurrences(of: "%s", with: "$$\(Date.riskyDateFormatter.string(from: riskyPositive.riskyEncounterDate))$$").attributedStringBold(color: UIColor.redButtonText)

        }
    }
}

extension TemplateViewController : OnboardingAnimatable {
    var contentViewController: UIViewController {
        return self.currentViewController ?? UIViewController()
    }
    
    var animateControl: UIControl {
        if let avc = self.currentViewController as? OnboardingAnimatable {
            return avc.animateControl
        }
        return UIControl()
    }
}

extension TemplateViewController {
    
    static func createViewController(basedOn state: TemplateControllerState) -> BaseViewController {
        
        switch state {
        case .FirstActivation:
            let vc = StoryboardScene.Onboarding.initialScene.instantiate()
            return vc
        case .ExposurePermissions:
            let vc = StoryboardScene.Onboarding.exposurePermissions.instantiate()
            return vc
        case .Privacy:
            let vc = StoryboardScene.Onboarding.privacy.instantiate()
            return vc
        case .Home:
            let vc = StoryboardScene.Home.initialScene.instantiate()
            return vc
        case .RiskyEncountersNegative:
            let vc = StoryboardScene.Risky.negativeScene.instantiate()
            return vc
        case .RiskyEncountersPositive:
            let vc = StoryboardScene.Risky.positiveScene.instantiate()
            return vc
        case .SendReports:
            let vc = StoryboardScene.SendReports.initialScene.instantiate()
            return vc
        case .TaC:
            let vc = StoryboardScene.Tac.initialScene.instantiate()
            return vc
        case .About:
            let vc = StoryboardScene.About.initialScene.instantiate()
            return vc
        case .Help:
            let vc = StoryboardScene.Help.initialScene.instantiate()
            return vc
        case .ForceUpdateMandatory,
             .ForceUpdateOS,
             .ForceUpdateUnsupported,
             .ForceUpdateRetryConnection:
            let vc = StoryboardScene.ForceUpdate.forceUpdateVC.instantiate()
            vc.state = state
            return vc
        default:
            return BaseViewController()
        }
    }
    
    func defaultNavBarHeight(basedOn state: TemplateControllerState) -> CGFloat {
        
        switch state {
        case .Home:
            return 104
        case .RiskyEncountersPositive:
            return 188
        case .RiskyEncountersNegative:
            return 80
        default:
            return 160
        }
    }
}


extension TemplateViewController {
    
    static func viewController(_ state: TemplateControllerState,
                               dateToShow: Date? = nil,
                               exposuresToShow: [Date]? = nil) -> TemplateViewController {
        
        let storyboard = UIStoryboard(name: "TemplateViewController", bundle: nil)
        if let tvc = storyboard.instantiateInitialViewController() as? TemplateViewController {
            tvc.state = state
            let vc = TemplateViewController.createViewController(basedOn: state)
            if let dtsh = dateToShow, let etsh = exposuresToShow, let repvc = vc as? RiskyViewControllerPositive {
                repvc.exposuresToShow = etsh
                repvc.riskyEncounterDate = dtsh
            }
            tvc.currentViewController = vc
            return tvc
        }
        return TemplateViewController()
    }
}

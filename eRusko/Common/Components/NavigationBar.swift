//
//  NavigationBar.swift
//  eRusko
//
//  Created by Slovensko IT on 22/10/2020.
//

import UIKit

final class NavigationBar: UIView, UIScrollViewDelegate {
        
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var redRoundedBannerView: UIView!
    @IBOutlet weak var calendarImageView: UIImageView!
    @IBOutlet weak var bannerTitleLabel: UILabel!
    
    
    @IBOutlet weak var leftTitleMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightTitleMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomTitleMarginConstraint: NSLayoutConstraint!
    
    var navigationBarHeightConstraint: NSLayoutConstraint?
    var deviceViewWidth: CGFloat = 320
    
    
    var animating: Bool = false

    var state: NavigationBarState = .titleIcon {
        didSet {
            updateVisual()
        }
    }
    var iconState: NavigationBarIconState = .forceUpdate {
        didSet {
            updateIconVisual()
        }
    }
    
    var backButtonCompletion: (() -> Void)?
    var rightButtonCompletion: (() -> Void)?
    var navigationControl: UINavigationController?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initSubViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initSubViews()
    }
    
    private func initSubViews() {
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: Bundle(for: type(of: self)))
        nib.instantiate(withOwner: self, options: nil)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(containerView)
        self.addConstraints()
        updateVisual()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
                                        self.topAnchor.constraint(equalTo: containerView.topAnchor),
                                        self.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
                                        self.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
                                        self.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
    
    
    @IBAction func rightButtonAction(_ sender: UIButton) {
        
        if rightButtonCompletion != nil { rightButtonCompletion?() }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        
        if backButtonCompletion != nil { backButtonCompletion?() }
    }
    
    func setState(_ state: NavigationBarState) {
        self.state = state
    }
    
    func updateVisual() {
        
        self.backButton.setTitle(AppConfig.shared.localized.generalBack!)
        self.containerView.backgroundColor = UIColor.navigationBarBackgroundColor
        setRiskyBannerVisual()
        
        switch state {
        case .titleIcon:
            titleLabel.isHidden = false
            rightButton.isHidden = true
            rightImageView.isHidden = false
            backButton.isHidden = true
            gradientView.isHidden = true
            redRoundedBannerView.isHidden = true
            break
        case .titleIconBackButton:
            titleLabel.isHidden = false
            rightButton.isHidden = true
            rightImageView.isHidden = false
            backButton.isHidden = false
            gradientView.isHidden = true
            redRoundedBannerView.isHidden = true
            break
        case .titleRightButton:
            titleLabel.isHidden = false
            rightButton.isHidden = false
            rightImageView.isHidden = true
            backButton.isHidden = true
            gradientView.isHidden = true
            redRoundedBannerView.isHidden = true
            break
        case .titleBackButton:
            titleLabel.isHidden = false
            rightButton.isHidden = true
            rightImageView.isHidden = true
            backButton.isHidden = false
            gradientView.isHidden = true
            redRoundedBannerView.isHidden = true
            break
        case .middleIcon:
            titleLabel.isHidden = true
            rightButton.isHidden = true
            rightImageView.isHidden = true
            backButton.isHidden = true
            gradientView.isHidden = true
            redRoundedBannerView.isHidden = true
            break
        case .middleIconBackButton:
            titleLabel.isHidden = true
            rightButton.isHidden = true
            rightImageView.isHidden = true
            backButton.isHidden = false
            gradientView.isHidden = true
            redRoundedBannerView.isHidden = true
            break
        case .riskyBanner:
            titleLabel.isHidden = true
            rightButton.isHidden = true
            rightImageView.isHidden = true
            backButton.isHidden = false
            gradientView.isHidden = false
            redRoundedBannerView.isHidden = false
            self.containerView.backgroundColor = UIColor.white
            break
        case .backButton:
            titleLabel.isHidden = true
            rightButton.isHidden = true
            rightImageView.isHidden = true
            backButton.isHidden = false
            gradientView.isHidden = true
            redRoundedBannerView.isHidden = true
            break
        }
        
    }
    
    func setIconState(_ state: NavigationBarIconState) {
        self.iconState = state
    }
    
    func updateIconVisual() {
        
        switch iconState {
        case .forceUpdate:
            rightImageView.image = UIImage(named: "iconfinder_mobile_call_phone_5964548")
            break
        case .firstActivation:
            rightImageView.image = UIImage(named: "iconfinder___avoid_public_crowd_5925235")

            break
        case .exposurePermissions:
            rightImageView.image = UIImage(named: "iconfinder_coronovirus_rumors_spread_disinformation_5932583")

            break
        case .privacy:
            rightImageView.image = UIImage(named: "iconfinder_home_office_work_5964551")

            break
        case .riskyNegative:

            break
        case .riskyPositive:

            break
        case .howItWorks:
            rightImageView.image = UIImage(named: "iconfinder_Artboard_12_5964541")

            break
        case .sendReports:
            rightImageView.image = UIImage(named: "iconfinder___avoid_public_crowd_5925235")
            break
        case .none:
            rightImageView.image = nil
            break
        }
    }
    
    func setRiskyBannerVisual() {
        
        self.redRoundedBannerView.layer.cornerRadius = 13
        self.redRoundedBannerView.backgroundColor = UIColor.redButtonBackground
        self.bannerTitleLabel.textColor = UIColor.redButtonText
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.redButtonBackground.cgColor, UIColor.white.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        gradientLayer.name = "gradientLayer"
        
        _ = gradientView.layer.sublayers?.map {
                if $0.name == "gradientLayer" {
                   $0.removeFromSuperlayer()
               }
            }
        self.gradientView.layer.insertSublayer(gradientLayer, at: 10)
        self.gradientView.layer.masksToBounds = true
    }
    
    func manage(basedOn state: TemplateControllerState) {
        
        switch state {
        case .FirstActivation:
            self.setState(.titleIcon)
            self.setIconState(.firstActivation)
            self.titleLabel.text = AppConfig.shared.localized.introToolbarTitle!
            break
        case .ExposurePermissions:
            self.setState(.titleIconBackButton)
            self.setIconState(.exposurePermissions)
            self.titleLabel.text = AppConfig.shared.localized.activationToolbarTitle!
            break
        case .Privacy:
            self.setState(.titleIconBackButton)
            self.setIconState(.privacy)
            self.titleLabel.text = AppConfig.shared.localized.privacyToolbarTitle!
            break
        case .Home:
            self.setState(.titleRightButton)
            self.titleLabel.text = AppConfig.shared.localized.dashboardToolbarTitle
            self.rightButton.setTitle(AppConfig.shared.localized.dashboardShare!)
            break
        case .RiskyEncountersNegative:
            self.setState(.backButton)
            self.setIconState(.riskyNegative)
            break
        case .RiskyEncountersPositive:
            self.setState(.riskyBanner)
            self.setIconState(.riskyPositive)
            break
        case .SendReports:
            self.setState(.titleIconBackButton)
            self.setIconState(.sendReports)
            self.titleLabel.text = AppConfig.shared.localized.submitKeysToolbarTitle!
            break
        case .TaC:
            self.setState(.titleIconBackButton)
            self.setIconState(.none)
            self.titleLabel.text = AppConfig.shared.localized.tacToolbarTitle
            break
        case .About:
            self.setState(.titleBackButton)
            self.setIconState(.none)
            self.titleLabel.text = AppConfig.shared.localized.infoToolbarTitle ?? ""
            break
        case .Help:
            self.setState(.titleIconBackButton)
            self.setIconState(.howItWorks)
            self.titleLabel.text = AppConfig.shared.localized.aboutToolbarTitle
            break
        case .ForceUpdateMandatory:
            self.setState(.titleIcon)
            self.setIconState(.forceUpdate)
            self.titleLabel.text = AppConfig.shared.localized.mandatoryUpdateTitle
            break
        case .ForceUpdateOS:
            self.setState(.titleIcon)
            self.setIconState(.forceUpdate)
            self.titleLabel.text = AppConfig.shared.localized.mandatoryUpdateTitle
            break
        case .ForceUpdateUnsupported:
            self.setState(.titleIcon)
            self.setIconState(.forceUpdate)
            self.titleLabel.text = AppConfig.shared.localized.unsupportedDeviceTitle
            break
        case .ForceUpdateRetryConnection:
            self.setState(.titleIcon)
            self.setIconState(.forceUpdate)
            self.titleLabel.text = AppConfig.shared.localized.appFirstStartConfigErrorToolbarTitle
            break
        default:
            break
        }
    }
}


extension NavigationBar {
    
    enum NavigationBarState {
        case titleIcon
        case titleIconBackButton
        case titleRightButton
        case titleBackButton
        case middleIcon
        case middleIconBackButton
        case riskyBanner
        case backButton
    }
    
    enum NavigationBarIconState {
        
        case forceUpdate
        case firstActivation
        case exposurePermissions
        case privacy
        case riskyNegative
        case riskyPositive
        case howItWorks
        case sendReports
        case none
    }
}


extension NavigationBar {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0)
        {
            if scrollView.contentOffset.y < 15 {
                animateBar(true)
            }
        }
       else
       {
            animateBar(false)
       }
    }
    
    func animateBar(_ up: Bool) {
        if animating { return }
        
        let maxBarHeight: CGFloat = 160
        let minBarHeight: CGFloat = 80
        
        animating = true
                
        self.navigationBarHeightConstraint?.constant = up ? maxBarHeight : minBarHeight
        self.leftTitleMarginConstraint.constant = up ? 20 : (20 + self.backButton.frame.width + 10)
        self.rightTitleMarginConstraint.constant = up ? 120 : (20 + self.backButton.frame.width + 10)
        self.bottomTitleMarginConstraint.constant = up ? 20 : 10
        
        UIView.animate(withDuration: 0.4) {
            self.rightImageView.alpha = up ? 1 : 0
            self.titleLabel.numberOfLines = up ? 2 : 1
            self.superview?.layoutIfNeeded()
            
        } completion: { (finished) in
            self.animating = false
        }
    }
}

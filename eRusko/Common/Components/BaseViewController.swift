//
//  BaseViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit

class BaseViewController: UIViewController {
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func dimIn() {
        
        UIView.animate(withDuration: 0.5, delay: 0.2, animations: {
            self.view.backgroundColor = UIColor(white:0, alpha: 0.2)
        })
    }
    
    func dimOut() {
        
        self.view.backgroundColor = UIColor.clear
    }
}

protocol ScrollAble: UIViewController {
    
    var scrollingControl: UIScrollView { get }
}

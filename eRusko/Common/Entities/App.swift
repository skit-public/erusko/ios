//
//  App.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import Foundation

struct App {
    static var appVersion: Version {
        let rawValue = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        return Version(rawValue ?? "")
    }

    static var bundleBuild: String {
        Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String ?? ""
    }
}

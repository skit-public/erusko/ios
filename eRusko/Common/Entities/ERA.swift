//
//  ERAnalytics.swift
//  eRusko
//
//  Created by Slovensko IT on 04/11/2020.
//

import Foundation

enum ERA: String {
    
    case app_os_version_error  // iOS version check 13.0-13.6
    case app_play_version_error // Play Version does not support EN API
    case app_mandatory_update_error // When Mandatory update screen is displayed
    case app_exposure_notifications_region_error // When other app is set in system Exposure Settings
    case app_activation_started // When user clicks start button
    case app_activation_finished // When user clicks finish button
    case app_activation_permission_denied // When user denies required EN permission
    case app_vc_deeplink_opened // When user verifies VC on screen 3.1 via deeplink
    case app_vc_manually_typed // When user clicks Verify button on screen 3.1 and VC was typed manually  
    case app_vc_verification_error // Error returned by Verification Server
    //code_invalid, code_expired, code_not_found
    case app_vc_verification_success // Verification Token returned by Verification Server
    case app_diagnostic_keys_permission_denied // User denied Key sharing
    case app_diagnostic_keys_error // User got error (network, ...)
    case app_diagnostic_keys_shared // User uploaded Diagnostic Keys to Key Server
    case app_exposure_detected // User got Exposure Notification for a day. MAx 1 event per day
    case app_share_app_clicked // User clicked Share app button on Main Screen
    case app_exposure_state // User States - not event //Active, Inactive
    case app_exposure_batch_download_started // App started batch download from Key Server
    case app_exposure_batch_download_finished // App finished batch download from Key Server
    
}

//
//  AppDependency.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import Foundation
import Firebase
import FirebaseFunctions

final class AppDependency {

    private(set) lazy var exposureService: ExposureServicing = ExposureService()

    private(set) lazy var reporter: ReportServicing = ReportService(
        serverUrl: AppConfig.shared.exposuresServer?.keyServerUrl ?? AppStorage.serverConfiguration?.keyServerUrl,
        exportUrl: AppConfig.shared.exposuresServer?.keyExportUrl ?? AppStorage.serverConfiguration?.keyExportUrl,
        healthAuthority: AppConfig.shared.exposuresServer?.healthAuthorityID ?? AppStorage.serverConfiguration?.healthAuthorityID
    )

    private(set) lazy var verification: VerificationServicing = VerificationService(
        url: AppConfig.shared.exposuresServer?.verificationServerUrl ?? AppStorage.serverConfiguration?.verificationServerUrl,
        deviceKey: AppConfig.shared.exposuresServer?.verificationServerApiKey ?? AppStorage.serverConfiguration?.verificationServerApiKey
    )

    private(set) lazy var background = BackgroundService(exposureService: exposureService, reporter: reporter)

}

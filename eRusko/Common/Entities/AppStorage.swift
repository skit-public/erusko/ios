//
//  AppStorage.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import Foundation
import FirebaseAuth

struct AppStorage {

    private enum Keys: String {
        
        case eTagsList
        case validationCodeFromSMS
        case exposures
        case backgroundModeAlertShown
        case lastProcessedFileName //TODO: remove this field, probably unused!
        case lastExposureWarningDate
        case activated
        case startedScanningAfterFirstRun
        case serverConfiguration
    }
    
    
    /// server Configuration from firebase
    static var serverConfiguration: ExposuresServerFBO? {
        get {
            guard let data = UserDefaults.standard.value(forKey: Keys.serverConfiguration.rawValue) as? Data else { return nil }
            guard let serverConf = try? PropertyListDecoder().decode(ExposuresServerFBO.self, from: data) else { return nil }
            return serverConf
        }
        set {
            guard let newValue = newValue else { return }
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: Keys.serverConfiguration.rawValue)
        }
    }

    /// startedScanningAfterFirstRun
    static var startedScanningAfterFirstRun: Bool {
        get {
            bool(forKey: .startedScanningAfterFirstRun)
        }
        set {
            set(withKey: .startedScanningAfterFirstRun, value: newValue)
        }
    }
    
    /// If background mode off alert was shown
    static var backgroundModeAlertShown: Bool {
        get {
            bool(forKey: .backgroundModeAlertShown)
        }
        set {
            set(withKey: .backgroundModeAlertShown, value: newValue)
        }
    }

    /// Last processed file name
    static var lastProcessedFileName: String? {
        get {
            string(forKey: .lastProcessedFileName)
        }
        set {
            set(withKey: .lastProcessedFileName, value: newValue)
        }
    }
    
    /// validation code from sms
    static var validationCodeFromSMS: String {
        get {
            string(forKey: .validationCodeFromSMS)
        }
        set {
            set(withKey: .validationCodeFromSMS, value: newValue)
        }
    }
    
    /// list of all eTags of zip files
    static var eTagsList: [String] {
        get {
            let arr = array(forKey: .eTagsList)
            if arr.count > 0 {
                return arr.map { (element) -> String in
                    if !(element is NSNull) {
                        return String(describing: element)
                    }
                    return ""
                }
            } else {
                return []
            }
        }
        set {
            set(withKey: .eTagsList, value: newValue)
        }
    }
    
    /// list of all exposures
    static var exposures: [Date] {
        get {
            arrayDates(forKey: .exposures)
        }
        set {
            set(withKey: .exposures, value: newValue)
        }
    }

    /// Last shown exposure warning date
    static var lastExposureWarningDate: Date? {
        get {
            date(forKey: .lastExposureWarningDate)
        }
        set {
            set(withKey: .lastExposureWarningDate, value: newValue)
        }
    }

    /// If current customToken value from Keychain is activated or needs to reactivate.
    /// Using this value for handling app reinstallation.
    static var activated: Bool {
        get {
            bool(forKey: .activated)
        }
        set {
            set(withKey: .activated, value: newValue)
        }
    }

    /// Cleanup data after logout
    static func deleteAllData() {
        activated = false

        backgroundModeAlertShown = false

//        state = nil

        lastProcessedFileName = nil
        eTagsList = []
//        lastUploadDate = nil
        
        lastExposureWarningDate = nil
        exposures = []

    }

    // MARK: - Private

    private static func bool(forKey key: Keys) -> Bool {
        return UserDefaults.standard.bool(forKey: key.rawValue)
    }

    private static func double(forKey key: Keys) -> Double {
        return UserDefaults.standard.double(forKey: key.rawValue)
    }

    private static func string(forKey key: Keys) -> String {
        return UserDefaults.standard.string(forKey: key.rawValue) ?? ""
    }
    
    private static func date(forKey key: Keys) -> Date? {
        return UserDefaults.standard.object(forKey: key.rawValue) as? Date
    }
    
    private static func array(forKey key: Keys) -> [Any] {
        return UserDefaults.standard.array(forKey: key.rawValue) ?? []
    }
    
    private static func arrayDates(forKey key: Keys) -> [Date] {
        return UserDefaults.standard.array(forKey: key.rawValue) as? [Date] ?? []
    }

    private static func set(withKey key: Keys, value: Any?) {
        UserDefaults.standard.set(value, forKey: key.rawValue)
    }
}

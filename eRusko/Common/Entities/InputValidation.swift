//
//  InputValidation.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit

enum InputValidation {
    case prefix, number, code

    var characterSet: CharacterSet {
        switch self {
        case .prefix:
            return CharacterSet(charactersIn: "+0123456789")
        case .number, .code:
            return CharacterSet(charactersIn: "0123456789")
        }
    }

    var rangeLimit: ClosedRange<Int> {
        switch self {
        case .prefix:
            return 2...5
        case .number:
            return 9...20
        case .code:
            return 4...18
        }
    }

    func validate(_ text: String) -> Bool {
        guard rangeLimit.contains(text.count), text == filtered(text) else { return false }
        return true
    }

    func filtered(_ text: String) -> String {
        let set = characterSet.inverted
        return text.components(separatedBy: set).joined()
    }

    func checkChange(_ oldString: String, _ newString: String) -> (result: Bool, edited: String?) {
        guard newString.count <= rangeLimit.upperBound else {
            let text = String(filtered(newString).prefix(rangeLimit.upperBound))
            return (result: false, edited: oldString == text ? nil : text)
        }
        return (result: true, edited: nil)
    }
}

extension UITextFieldDelegate {

    func validateTextChange(with type: InputValidation, textField: UITextField, changeCharactersIn range: NSRange, newString string: String) -> Bool {
        guard let text = textField.text else { return true }

        let candidate = NSString(string: text).replacingCharacters(in: range, with: string)
        let check = type.checkChange(text, candidate)
        if check.result {
            return true
        }
        DispatchQueue.main.async {
            textField.text = check.edited ?? text
            textField.sendActions(for: .valueChanged)
        }
        return false
    }

}

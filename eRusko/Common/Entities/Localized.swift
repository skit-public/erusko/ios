//
//  Localized.swift
//  Appka
//
//  Created by Slovensko IT on 29/10/2020.
//

import Foundation

struct Localized : Codable {
    var aboutBody : String?
    var aboutTitle : String?
    var aboutToolbarTitle : String?
    var activationNotificationsBody : String?
    var activationNotificationsTitle : String?
    var activationToolbarTitle : String?
    var activeBackgroundModeMessage : String?
    var activeBackgroundModeTitle : String?
    var appName : String?
    var connectionErrorMessage : String?
    var connectionErrorTitle : String?
    var dashboardBannerContact : String?
    var dashboardBannerNoContactLately : String?
    var dashboardBody : String?
    var dashboardPauseApp : String?
    var dashboardShare : String?
    var dashboardShareText : String?
    var dashboardStartApp : String?
    var dashboardSubmitButton : String?
    var dashboardTitleDisabled : String?
    var dashboardTitleEnabled : String?
    var dashboardToolbarTitle : String?
    var exposureActivationRestrictedBody : String?
    var exposureActivationRestrictedTitle : String?
    var exposureActivationStorageBody : String?
    var exposureActivationStorageTitle : String?
    var exposureActivationUnknownBody : String?
    var exposureActivationUnknownTitle : String?
    var exposureDeactivationUnknownBody : String?
    var exposureDeactivationUnknownTitle : String?
    var exposureErrorDeveloperErrorMessage : String?
    var exposureErrorDeveloperErrorTitle : String?
    var exposureErrorFailedBluetoothDisabledMessage : String?
    var exposureErrorFailedBluetoothDisabledTitle : String?
    var exposureErrorFailedDiskIoMessage : String?
    var exposureErrorFailedDiskIoTitle : String?
    var exposureErrorFailedNotSupportedMessage : String?
    var exposureErrorFailedNotSupportedTitle : String?
    var exposureErrorFailedServiceDisabledMessage : String?
    var exposureErrorFailedServiceDisabledTitle : String?
    var exposureErrorFailedTemporarilyDisabledMessage : String?
    var exposureErrorFailedTemporarilyDisabledTitle : String?
    var exposureErrorInvalidAccountMessage : String?
    var exposureErrorInvalidAccountTitle : String?
    var exposureErrorSignInRequiredMessage : String?
    var exposureErrorSignInRequiredTitle : String?
    var forceOsUpdateBody : String?
    var forceOsUpdateSubtitle : String?
    var generalBack : String?
    var generalClose : String?
    var generalEnable : String?
    var generalErrorMessage : String?
    var generalErrorTitle : String?
    var generalNoInternet : String?
    var generalOk : String?
    var goToSettingsAction : String?
    var infoBody : String?
    var infoToolbarTitle : String?
    var introBody : String?
    var introNextButton : String?
    var introTitle : String?
    var introToolbarTitle : String?
    var mainSymptomsBodyPain : String?
    var mainSymptomsCoughing : String?
    var mainSymptomsHighFever : String?
    var mainSymptomsSoreThroat : String?
    var mainSymptomsSubtitle : String?
    var mainSymptomsTitle : String?
    var mandatoryUpdateBody : String?
    var mandatoryUpdateButton : String?
    var mandatoryUpdateSubtitle : String?
    var mandatoryUpdateTitle : String?
    var notificationDataOutdatedBody : String?
    var notificationDataOutdatedTitle : String?
    var notificationDownloadingKeysBody : String?
    var notificationDownloadingKeysTitle : String?
    var notificationExposedBody : String?
    var notificationExposedTitle : String?
    var notificationScanningPausedBody : String?
    var notificationScanningPausedTitle : String?
    var notificationsChannelDefault : String?
    var notificationsChannelDisabled : String?
    var notificationsChannelExposures : String?
    var notificationsChannelOutdatedData : String?
    var notificationsChannelWorker : String?
    var playServicesErrorBody : String?
    var playServicesErrorButton : String?
    var playServicesErrorTitle : String?
    var privacyBody : String?
    var privacyButton : String?
    var privacyTitle : String?
    var privacyToolbarTitle : String?
    var recentEncountersBehaviour : String?
    var recentEncountersBody : String?
    var recentEncountersMainSymptoms : String?
    var recentEncountersNoExposureBody : String?
    var recentEncountersNoExposureTitle : String?
    var recentEncountersNoSymptomsBody : String?
    var recentEncountersNoSymptomsTitle : String?
    var recentEncountersRecent : String?
    var recentEncountersSymptomsBody : String?
    var recentEncountersSymptomsTitle : String?
    var recentEncountersTitle : String?
    var responsibleBehaviourCoughToCloth : String?
    var responsibleBehaviourDesinfectHands : String?
    var responsibleBehaviourKeepDistance : String?
    var responsibleBehaviourSubtitle : String?
    var responsibleBehaviourTitle : String?
    var responsibleBehaviourWashHands : String?
    var responsibleBehaviourWithFaceMask : String?
    var riskyEncountersDialogTitle : String?
    var submitKeysBody : String?
    var submitKeysButton : String?
    var submitKeysErrorCodeExpiredBody : String?
    var submitKeysErrorCodeExpiredTitle : String?
    var submitKeysErrorCodeInvalid : String?
    var submitKeysErrorCodeInvalidBody : String?
    var submitKeysErrorCodeInvalidTitle : String?
    var submitKeysErrorNoKeys : String?
    var submitKeysErrorRestrictionMessage : String?
    var submitKeysErrorRestrictionTitle : String?
    var submitKeysErrorSendFailedTitle : String?
    var submitKeysInputHint : String?
    var submitKeysSuccessBody : String?
    var submitKeysSuccessTitle : String?
    var submitKeysToolbarTitle : String?
    var submitKeysTryAgain : String?
    var tacBody : String?
    var tacTitle : String?
    var tacToolbarTitle : String?
    var unsupportedDeviceBody : String?
    var unsupportedDeviceButton : String?
    var unsupportedDeviceSubtitle : String?
    var unsupportedDeviceTitle : String?
    var androidAutostartPermissionsToolbarTitle : String?
    var androidAutostartPermissionsBody : String?
    var androidAutostartPermissionsNext : String?
    var androidAutostartPermissionsSettings : String?
    var androidPowersavingSettingsTitle : String?
    var androidPowersavingSettingsBody : String?
    var androidSettingsCouldNotOpenTitle : String?
    var androidSettingsCouldNotOpenBody : String?
    var appFirstStartConfigErrorBody : String?
    var appFirstStartConfigErrorTitle : String?
    var appFirstStartConfigErrorButton : String?
    var mandatoryUpdateToolbarTitle : String?
    var playServicesErrorToolbarTitle : String?
    var appFirstStartConfigErrorToolbarTitle : String?

    enum CodingKeys: String, CodingKey {
        case aboutBody = "about_body"
        case aboutTitle = "about_title"
        case aboutToolbarTitle = "about_toolbar_title"
        case activationNotificationsBody = "activation_notifications_body"
        case activationNotificationsTitle = "activation_notifications_title"
        case activationToolbarTitle = "activation_toolbar_title"
        case activeBackgroundModeMessage = "active_background_mode_message"
        case activeBackgroundModeTitle = "active_background_mode_title"
        case appName = "app_name"
        case connectionErrorMessage = "connection_error_message"
        case connectionErrorTitle = "connection_error_title"
        case dashboardBannerContact = "dashboard_banner_contact"
        case dashboardBannerNoContactLately = "dashboard_banner_no_contact_lately"
        case dashboardBody = "dashboard_body"
        case dashboardPauseApp = "dashboard_pause_app"
        case dashboardShare = "dashboard_share"
        case dashboardShareText = "dashboard_share_text"
        case dashboardStartApp = "dashboard_start_app"
        case dashboardSubmitButton = "dashboard_submit_button"
        case dashboardTitleDisabled = "dashboard_title_disabled"
        case dashboardTitleEnabled = "dashboard_title_enabled"
        case dashboardToolbarTitle = "dashboard_toolbar_title"
        case exposureActivationRestrictedBody = "exposure_activation_restricted_body"
        case exposureActivationRestrictedTitle = "exposure_activation_restricted_title"
        case exposureActivationStorageBody = "exposure_activation_storage_body"
        case exposureActivationStorageTitle = "exposure_activation_storage_title"
        case exposureActivationUnknownBody = "exposure_activation_unknown_body"
        case exposureActivationUnknownTitle = "exposure_activation_unknown_title"
        case exposureDeactivationUnknownBody = "exposure_deactivation_unknown_body"
        case exposureDeactivationUnknownTitle = "exposure_deactivation_unknown_title"
        case exposureErrorDeveloperErrorMessage = "exposure_error_developer_error_message"
        case exposureErrorDeveloperErrorTitle = "exposure_error_developer_error_title"
        case exposureErrorFailedBluetoothDisabledMessage = "exposure_error_failed_bluetooth_disabled_message"
        case exposureErrorFailedBluetoothDisabledTitle = "exposure_error_failed_bluetooth_disabled_title"
        case exposureErrorFailedDiskIoMessage = "exposure_error_failed_disk_io_message"
        case exposureErrorFailedDiskIoTitle = "exposure_error_failed_disk_io_title"
        case exposureErrorFailedNotSupportedMessage = "exposure_error_failed_not_supported_message"
        case exposureErrorFailedNotSupportedTitle = "exposure_error_failed_not_supported_title"
        case exposureErrorFailedServiceDisabledMessage = "exposure_error_failed_service_disabled_message"
        case exposureErrorFailedServiceDisabledTitle = "exposure_error_failed_service_disabled_title"
        case exposureErrorFailedTemporarilyDisabledMessage = "exposure_error_failed_temporarily_disabled_message"
        case exposureErrorFailedTemporarilyDisabledTitle = "exposure_error_failed_temporarily_disabled_title"
        case exposureErrorInvalidAccountMessage = "exposure_error_invalid_account_message"
        case exposureErrorInvalidAccountTitle = "exposure_error_invalid_account_title"
        case exposureErrorSignInRequiredMessage = "exposure_error_sign_in_required_message"
        case exposureErrorSignInRequiredTitle = "exposure_error_sign_in_required_title"
        case forceOsUpdateBody = "force_os_update_body"
        case forceOsUpdateSubtitle = "force_os_update_subtitle"
        case generalBack = "general_back"
        case generalClose = "general_close"
        case generalEnable = "general_enable"
        case generalErrorMessage = "general_error_message"
        case generalErrorTitle = "general_error_title"
        case generalNoInternet = "general_no_internet"
        case generalOk = "general_ok"
        case goToSettingsAction = "go_to_settings_action"
        case infoBody = "info_body"
        case infoToolbarTitle = "info_toolbar_title"
        case introBody = "intro_body"
        case introNextButton = "intro_next_button"
        case introTitle = "intro_title"
        case introToolbarTitle = "intro_toolbar_title"
        case mainSymptomsBodyPain = "main_symptoms_body_pain"
        case mainSymptomsCoughing = "main_symptoms_coughing"
        case mainSymptomsHighFever = "main_symptoms_high_fever"
        case mainSymptomsSoreThroat = "main_symptoms_sore_throat"
        case mainSymptomsSubtitle = "main_symptoms_subtitle"
        case mainSymptomsTitle = "main_symptoms_title"
        case mandatoryUpdateBody = "mandatory_update_body"
        case mandatoryUpdateButton = "mandatory_update_button"
        case mandatoryUpdateSubtitle = "mandatory_update_subtitle"
        case mandatoryUpdateTitle = "mandatory_update_title"
        case notificationDataOutdatedBody = "notification_data_outdated_body"
        case notificationDataOutdatedTitle = "notification_data_outdated_title"
        case notificationDownloadingKeysBody = "notification_downloading_keys_body"
        case notificationDownloadingKeysTitle = "notification_downloading_keys_title"
        case notificationExposedBody = "notification_exposed_body"
        case notificationExposedTitle = "notification_exposed_title"
        case notificationScanningPausedBody = "notification_scanning_paused_body"
        case notificationScanningPausedTitle = "notification_scanning_paused_title"
        case notificationsChannelDefault = "notifications_channel_default"
        case notificationsChannelDisabled = "notifications_channel_disabled"
        case notificationsChannelExposures = "notifications_channel_exposures"
        case notificationsChannelOutdatedData = "notifications_channel_outdated_data"
        case notificationsChannelWorker = "notifications_channel_worker"
        case playServicesErrorBody = "play_services_error_body"
        case playServicesErrorButton = "play_services_error_button"
        case playServicesErrorTitle = "play_services_error_title"
        case privacyBody = "privacy_body"
        case privacyButton = "privacy_button"
        case privacyTitle = "privacy_title"
        case privacyToolbarTitle = "privacy_toolbar_title"
        case recentEncountersBehaviour = "recent_encounters_behaviour"
        case recentEncountersBody = "recent_encounters_body"
        case recentEncountersMainSymptoms = "recent_encounters_main_symptoms"
        case recentEncountersNoExposureBody = "recent_encounters_no_exposure_body"
        case recentEncountersNoExposureTitle = "recent_encounters_no_exposure_title"
        case recentEncountersNoSymptomsBody = "recent_encounters_no_symptoms_body"
        case recentEncountersNoSymptomsTitle = "recent_encounters_no_symptoms_title"
        case recentEncountersRecent = "recent_encounters_recent"
        case recentEncountersSymptomsBody = "recent_encounters_symptoms_body"
        case recentEncountersSymptomsTitle = "recent_encounters_symptoms_title"
        case recentEncountersTitle = "recent_encounters_title"
        case responsibleBehaviourCoughToCloth = "responsible_behaviour_cough_to_cloth"
        case responsibleBehaviourDesinfectHands = "responsible_behaviour_desinfect_hands"
        case responsibleBehaviourKeepDistance = "responsible_behaviour_keep_distance"
        case responsibleBehaviourSubtitle = "responsible_behaviour_subtitle"
        case responsibleBehaviourTitle = "responsible_behaviour_title"
        case responsibleBehaviourWashHands = "responsible_behaviour_wash_hands"
        case responsibleBehaviourWithFaceMask = "responsible_behaviour_with_face_mask"
        case riskyEncountersDialogTitle = "risky_encounters_dialog_title"
        case submitKeysBody = "submit_keys_body"
        case submitKeysButton = "submit_keys_button"
        case submitKeysErrorCodeExpiredBody = "submit_keys_error_code_expired_body"
        case submitKeysErrorCodeExpiredTitle = "submit_keys_error_code_expired_title"
        case submitKeysErrorCodeInvalid = "submit_keys_error_code_invalid"
        case submitKeysErrorCodeInvalidBody = "submit_keys_error_code_invalid_body"
        case submitKeysErrorCodeInvalidTitle = "submit_keys_error_code_invalid_title"
        case submitKeysErrorNoKeys = "submit_keys_error_no_keys"
        case submitKeysErrorRestrictionMessage = "submit_keys_error_restriction_message"
        case submitKeysErrorRestrictionTitle = "submit_keys_error_restriction_title"
        case submitKeysErrorSendFailedTitle = "submit_keys_error_send_failed_title"
        case submitKeysInputHint = "submit_keys_input_hint"
        case submitKeysSuccessBody = "submit_keys_success_body"
        case submitKeysSuccessTitle = "submit_keys_success_title"
        case submitKeysToolbarTitle = "submit_keys_toolbar_title"
        case submitKeysTryAgain = "submit_keys_try_again"
        case tacBody = "tac_body"
        case tacTitle = "tac_title"
        case tacToolbarTitle = "tac_toolbar_title"
        case unsupportedDeviceBody = "unsupported_device_body"
        case unsupportedDeviceButton = "unsupported_device_button"
        case unsupportedDeviceSubtitle = "unsupported_device_subtitle"
        case unsupportedDeviceTitle = "unsupported_device_title"
        case androidAutostartPermissionsToolbarTitle = "android_autostart_permissions_toolbar_title"
        case androidAutostartPermissionsBody = "android_autostart_permissions_body"
        case androidAutostartPermissionsNext = "android_autostart_permissions_next"
        case androidAutostartPermissionsSettings = "android_autostart_permissions_settings"
        case androidPowersavingSettingsTitle = "android_powersaving_settings_title"
        case androidPowersavingSettingsBody = "android_powersaving_settings_body"
        case androidSettingsCouldNotOpenTitle = "android_settings_could_not_open_title"
        case androidSettingsCouldNotOpenBody = "android_settings_could_not_open_body"
        case appFirstStartConfigErrorBody = "app_first_start_config_error_body"
        case appFirstStartConfigErrorTitle = "app_first_start_config_error_title"
        case appFirstStartConfigErrorButton = "app_first_start_config_error_button"
        case mandatoryUpdateToolbarTitle = "mandatory_update_toolbar_title"
        case playServicesErrorToolbarTitle = "play_services_error_toolbar_title"
        case appFirstStartConfigErrorToolbarTitle = "app_first_start_config_error_toolbar_title"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        aboutBody = try values.decodeIfPresent(String.self, forKey: .aboutBody)
        aboutTitle = try values.decodeIfPresent(String.self, forKey: .aboutTitle)
        aboutToolbarTitle = try values.decodeIfPresent(String.self, forKey: .aboutToolbarTitle)
        activationNotificationsBody = try values.decodeIfPresent(String.self, forKey: .activationNotificationsBody)
        activationNotificationsTitle = try values.decodeIfPresent(String.self, forKey: .activationNotificationsTitle)
        activationToolbarTitle = try values.decodeIfPresent(String.self, forKey: .activationToolbarTitle)
        activeBackgroundModeMessage = try values.decodeIfPresent(String.self, forKey: .activeBackgroundModeMessage)
        activeBackgroundModeTitle = try values.decodeIfPresent(String.self, forKey: .activeBackgroundModeTitle)
        appName = try values.decodeIfPresent(String.self, forKey: .appName)
        connectionErrorMessage = try values.decodeIfPresent(String.self, forKey: .connectionErrorMessage)
        connectionErrorTitle = try values.decodeIfPresent(String.self, forKey: .connectionErrorTitle)
        dashboardBannerContact = try values.decodeIfPresent(String.self, forKey: .dashboardBannerContact)
        dashboardBannerNoContactLately = try values.decodeIfPresent(String.self, forKey: .dashboardBannerNoContactLately)
        dashboardBody = try values.decodeIfPresent(String.self, forKey: .dashboardBody)
        dashboardPauseApp = try values.decodeIfPresent(String.self, forKey: .dashboardPauseApp)
        dashboardShare = try values.decodeIfPresent(String.self, forKey: .dashboardShare)
        dashboardShareText = try values.decodeIfPresent(String.self, forKey: .dashboardShareText)
        dashboardStartApp = try values.decodeIfPresent(String.self, forKey: .dashboardStartApp)
        dashboardSubmitButton = try values.decodeIfPresent(String.self, forKey: .dashboardSubmitButton)
        dashboardTitleDisabled = try values.decodeIfPresent(String.self, forKey: .dashboardTitleDisabled)
        dashboardTitleEnabled = try values.decodeIfPresent(String.self, forKey: .dashboardTitleEnabled)
        dashboardToolbarTitle = try values.decodeIfPresent(String.self, forKey: .dashboardToolbarTitle)
        exposureActivationRestrictedBody = try values.decodeIfPresent(String.self, forKey: .exposureActivationRestrictedBody)
        exposureActivationRestrictedTitle = try values.decodeIfPresent(String.self, forKey: .exposureActivationRestrictedTitle)
        exposureActivationStorageBody = try values.decodeIfPresent(String.self, forKey: .exposureActivationStorageBody)
        exposureActivationStorageTitle = try values.decodeIfPresent(String.self, forKey: .exposureActivationStorageTitle)
        exposureActivationUnknownBody = try values.decodeIfPresent(String.self, forKey: .exposureActivationUnknownBody)
        exposureActivationUnknownTitle = try values.decodeIfPresent(String.self, forKey: .exposureActivationUnknownTitle)
        exposureDeactivationUnknownBody = try values.decodeIfPresent(String.self, forKey: .exposureDeactivationUnknownBody)
        exposureDeactivationUnknownTitle = try values.decodeIfPresent(String.self, forKey: .exposureDeactivationUnknownTitle)
        exposureErrorDeveloperErrorMessage = try values.decodeIfPresent(String.self, forKey: .exposureErrorDeveloperErrorMessage)
        exposureErrorDeveloperErrorTitle = try values.decodeIfPresent(String.self, forKey: .exposureErrorDeveloperErrorTitle)
        exposureErrorFailedBluetoothDisabledMessage = try values.decodeIfPresent(String.self, forKey: .exposureErrorFailedBluetoothDisabledMessage)
        exposureErrorFailedBluetoothDisabledTitle = try values.decodeIfPresent(String.self, forKey: .exposureErrorFailedBluetoothDisabledTitle)
        exposureErrorFailedDiskIoMessage = try values.decodeIfPresent(String.self, forKey: .exposureErrorFailedDiskIoMessage)
        exposureErrorFailedDiskIoTitle = try values.decodeIfPresent(String.self, forKey: .exposureErrorFailedDiskIoTitle)
        exposureErrorFailedNotSupportedMessage = try values.decodeIfPresent(String.self, forKey: .exposureErrorFailedNotSupportedMessage)
        exposureErrorFailedNotSupportedTitle = try values.decodeIfPresent(String.self, forKey: .exposureErrorFailedNotSupportedTitle)
        exposureErrorFailedServiceDisabledMessage = try values.decodeIfPresent(String.self, forKey: .exposureErrorFailedServiceDisabledMessage)
        exposureErrorFailedServiceDisabledTitle = try values.decodeIfPresent(String.self, forKey: .exposureErrorFailedServiceDisabledTitle)
        exposureErrorFailedTemporarilyDisabledMessage = try values.decodeIfPresent(String.self, forKey: .exposureErrorFailedTemporarilyDisabledMessage)
        exposureErrorFailedTemporarilyDisabledTitle = try values.decodeIfPresent(String.self, forKey: .exposureErrorFailedTemporarilyDisabledTitle)
        exposureErrorInvalidAccountMessage = try values.decodeIfPresent(String.self, forKey: .exposureErrorInvalidAccountMessage)
        exposureErrorInvalidAccountTitle = try values.decodeIfPresent(String.self, forKey: .exposureErrorInvalidAccountTitle)
        exposureErrorSignInRequiredMessage = try values.decodeIfPresent(String.self, forKey: .exposureErrorSignInRequiredMessage)
        exposureErrorSignInRequiredTitle = try values.decodeIfPresent(String.self, forKey: .exposureErrorSignInRequiredTitle)
        forceOsUpdateBody = try values.decodeIfPresent(String.self, forKey: .forceOsUpdateBody)
        forceOsUpdateSubtitle = try values.decodeIfPresent(String.self, forKey: .forceOsUpdateSubtitle)
        generalBack = try values.decodeIfPresent(String.self, forKey: .generalBack)
        generalClose = try values.decodeIfPresent(String.self, forKey: .generalClose)
        generalEnable = try values.decodeIfPresent(String.self, forKey: .generalEnable)
        generalErrorMessage = try values.decodeIfPresent(String.self, forKey: .generalErrorMessage)
        generalErrorTitle = try values.decodeIfPresent(String.self, forKey: .generalErrorTitle)
        generalNoInternet = try values.decodeIfPresent(String.self, forKey: .generalNoInternet)
        generalOk = try values.decodeIfPresent(String.self, forKey: .generalOk)
        goToSettingsAction = try values.decodeIfPresent(String.self, forKey: .goToSettingsAction)
        infoBody = try values.decodeIfPresent(String.self, forKey: .infoBody)
        infoToolbarTitle = try values.decodeIfPresent(String.self, forKey: .infoToolbarTitle)
        introBody = try values.decodeIfPresent(String.self, forKey: .introBody)
        introNextButton = try values.decodeIfPresent(String.self, forKey: .introNextButton)
        introTitle = try values.decodeIfPresent(String.self, forKey: .introTitle)
        introToolbarTitle = try values.decodeIfPresent(String.self, forKey: .introToolbarTitle)
        mainSymptomsBodyPain = try values.decodeIfPresent(String.self, forKey: .mainSymptomsBodyPain)
        mainSymptomsCoughing = try values.decodeIfPresent(String.self, forKey: .mainSymptomsCoughing)
        mainSymptomsHighFever = try values.decodeIfPresent(String.self, forKey: .mainSymptomsHighFever)
        mainSymptomsSoreThroat = try values.decodeIfPresent(String.self, forKey: .mainSymptomsSoreThroat)
        mainSymptomsSubtitle = try values.decodeIfPresent(String.self, forKey: .mainSymptomsSubtitle)
        mainSymptomsTitle = try values.decodeIfPresent(String.self, forKey: .mainSymptomsTitle)
        mandatoryUpdateBody = try values.decodeIfPresent(String.self, forKey: .mandatoryUpdateBody)
        mandatoryUpdateButton = try values.decodeIfPresent(String.self, forKey: .mandatoryUpdateButton)
        mandatoryUpdateSubtitle = try values.decodeIfPresent(String.self, forKey: .mandatoryUpdateSubtitle)
        mandatoryUpdateTitle = try values.decodeIfPresent(String.self, forKey: .mandatoryUpdateTitle)
        notificationDataOutdatedBody = try values.decodeIfPresent(String.self, forKey: .notificationDataOutdatedBody)
        notificationDataOutdatedTitle = try values.decodeIfPresent(String.self, forKey: .notificationDataOutdatedTitle)
        notificationDownloadingKeysBody = try values.decodeIfPresent(String.self, forKey: .notificationDownloadingKeysBody)
        notificationDownloadingKeysTitle = try values.decodeIfPresent(String.self, forKey: .notificationDownloadingKeysTitle)
        notificationExposedBody = try values.decodeIfPresent(String.self, forKey: .notificationExposedBody)
        notificationExposedTitle = try values.decodeIfPresent(String.self, forKey: .notificationExposedTitle)
        notificationScanningPausedBody = try values.decodeIfPresent(String.self, forKey: .notificationScanningPausedBody)
        notificationScanningPausedTitle = try values.decodeIfPresent(String.self, forKey: .notificationScanningPausedTitle)
        notificationsChannelDefault = try values.decodeIfPresent(String.self, forKey: .notificationsChannelDefault)
        notificationsChannelDisabled = try values.decodeIfPresent(String.self, forKey: .notificationsChannelDisabled)
        notificationsChannelExposures = try values.decodeIfPresent(String.self, forKey: .notificationsChannelExposures)
        notificationsChannelOutdatedData = try values.decodeIfPresent(String.self, forKey: .notificationsChannelOutdatedData)
        notificationsChannelWorker = try values.decodeIfPresent(String.self, forKey: .notificationsChannelWorker)
        playServicesErrorBody = try values.decodeIfPresent(String.self, forKey: .playServicesErrorBody)
        playServicesErrorButton = try values.decodeIfPresent(String.self, forKey: .playServicesErrorButton)
        playServicesErrorTitle = try values.decodeIfPresent(String.self, forKey: .playServicesErrorTitle)
        privacyBody = try values.decodeIfPresent(String.self, forKey: .privacyBody)
        privacyButton = try values.decodeIfPresent(String.self, forKey: .privacyButton)
        privacyTitle = try values.decodeIfPresent(String.self, forKey: .privacyTitle)
        privacyToolbarTitle = try values.decodeIfPresent(String.self, forKey: .privacyToolbarTitle)
        recentEncountersBehaviour = try values.decodeIfPresent(String.self, forKey: .recentEncountersBehaviour)
        recentEncountersBody = try values.decodeIfPresent(String.self, forKey: .recentEncountersBody)
        recentEncountersMainSymptoms = try values.decodeIfPresent(String.self, forKey: .recentEncountersMainSymptoms)
        recentEncountersNoExposureBody = try values.decodeIfPresent(String.self, forKey: .recentEncountersNoExposureBody)
        recentEncountersNoExposureTitle = try values.decodeIfPresent(String.self, forKey: .recentEncountersNoExposureTitle)
        recentEncountersNoSymptomsBody = try values.decodeIfPresent(String.self, forKey: .recentEncountersNoSymptomsBody)
        recentEncountersNoSymptomsTitle = try values.decodeIfPresent(String.self, forKey: .recentEncountersNoSymptomsTitle)
        recentEncountersRecent = try values.decodeIfPresent(String.self, forKey: .recentEncountersRecent)
        recentEncountersSymptomsBody = try values.decodeIfPresent(String.self, forKey: .recentEncountersSymptomsBody)
        recentEncountersSymptomsTitle = try values.decodeIfPresent(String.self, forKey: .recentEncountersSymptomsTitle)
        recentEncountersTitle = try values.decodeIfPresent(String.self, forKey: .recentEncountersTitle)
        responsibleBehaviourCoughToCloth = try values.decodeIfPresent(String.self, forKey: .responsibleBehaviourCoughToCloth)
        responsibleBehaviourDesinfectHands = try values.decodeIfPresent(String.self, forKey: .responsibleBehaviourDesinfectHands)
        responsibleBehaviourKeepDistance = try values.decodeIfPresent(String.self, forKey: .responsibleBehaviourKeepDistance)
        responsibleBehaviourSubtitle = try values.decodeIfPresent(String.self, forKey: .responsibleBehaviourSubtitle)
        responsibleBehaviourTitle = try values.decodeIfPresent(String.self, forKey: .responsibleBehaviourTitle)
        responsibleBehaviourWashHands = try values.decodeIfPresent(String.self, forKey: .responsibleBehaviourWashHands)
        responsibleBehaviourWithFaceMask = try values.decodeIfPresent(String.self, forKey: .responsibleBehaviourWithFaceMask)
        riskyEncountersDialogTitle = try values.decodeIfPresent(String.self, forKey: .riskyEncountersDialogTitle)
        submitKeysBody = try values.decodeIfPresent(String.self, forKey: .submitKeysBody)
        submitKeysButton = try values.decodeIfPresent(String.self, forKey: .submitKeysButton)
        submitKeysErrorCodeExpiredBody = try values.decodeIfPresent(String.self, forKey: .submitKeysErrorCodeExpiredBody)
        submitKeysErrorCodeExpiredTitle = try values.decodeIfPresent(String.self, forKey: .submitKeysErrorCodeExpiredTitle)
        submitKeysErrorCodeInvalid = try values.decodeIfPresent(String.self, forKey: .submitKeysErrorCodeInvalid)
        submitKeysErrorCodeInvalidBody = try values.decodeIfPresent(String.self, forKey: .submitKeysErrorCodeInvalidBody)
        submitKeysErrorCodeInvalidTitle = try values.decodeIfPresent(String.self, forKey: .submitKeysErrorCodeInvalidTitle)
        submitKeysErrorNoKeys = try values.decodeIfPresent(String.self, forKey: .submitKeysErrorNoKeys)
        submitKeysErrorRestrictionMessage = try values.decodeIfPresent(String.self, forKey: .submitKeysErrorRestrictionMessage)
        submitKeysErrorRestrictionTitle = try values.decodeIfPresent(String.self, forKey: .submitKeysErrorRestrictionTitle)
        submitKeysErrorSendFailedTitle = try values.decodeIfPresent(String.self, forKey: .submitKeysErrorSendFailedTitle)
        submitKeysInputHint = try values.decodeIfPresent(String.self, forKey: .submitKeysInputHint)
        submitKeysSuccessBody = try values.decodeIfPresent(String.self, forKey: .submitKeysSuccessBody)
        submitKeysSuccessTitle = try values.decodeIfPresent(String.self, forKey: .submitKeysSuccessTitle)
        submitKeysToolbarTitle = try values.decodeIfPresent(String.self, forKey: .submitKeysToolbarTitle)
        submitKeysTryAgain = try values.decodeIfPresent(String.self, forKey: .submitKeysTryAgain)
        tacBody = try values.decodeIfPresent(String.self, forKey: .tacBody)
        tacTitle = try values.decodeIfPresent(String.self, forKey: .tacTitle)
        tacToolbarTitle = try values.decodeIfPresent(String.self, forKey: .tacToolbarTitle)
        unsupportedDeviceBody = try values.decodeIfPresent(String.self, forKey: .unsupportedDeviceBody)
        unsupportedDeviceButton = try values.decodeIfPresent(String.self, forKey: .unsupportedDeviceButton)
        unsupportedDeviceSubtitle = try values.decodeIfPresent(String.self, forKey: .unsupportedDeviceSubtitle)
        unsupportedDeviceTitle = try values.decodeIfPresent(String.self, forKey: .unsupportedDeviceTitle)
        androidAutostartPermissionsToolbarTitle = try values.decodeIfPresent(String.self, forKey: .androidAutostartPermissionsToolbarTitle)
        androidAutostartPermissionsBody = try values.decodeIfPresent(String.self, forKey: .androidAutostartPermissionsBody)
        androidAutostartPermissionsNext = try values.decodeIfPresent(String.self, forKey: .androidAutostartPermissionsNext)
        androidAutostartPermissionsSettings = try values.decodeIfPresent(String.self, forKey: .androidAutostartPermissionsSettings)
        androidPowersavingSettingsTitle = try values.decodeIfPresent(String.self, forKey: .androidPowersavingSettingsTitle)
        androidPowersavingSettingsBody = try values.decodeIfPresent(String.self, forKey: .androidPowersavingSettingsBody)
        androidSettingsCouldNotOpenTitle = try values.decodeIfPresent(String.self, forKey: .androidSettingsCouldNotOpenTitle)
        androidSettingsCouldNotOpenBody = try values.decodeIfPresent(String.self, forKey: .androidSettingsCouldNotOpenBody)
        appFirstStartConfigErrorBody = try values.decodeIfPresent(String.self, forKey: .appFirstStartConfigErrorBody)
        appFirstStartConfigErrorTitle = try values.decodeIfPresent(String.self, forKey: .appFirstStartConfigErrorTitle)
        appFirstStartConfigErrorButton = try values.decodeIfPresent(String.self, forKey: .appFirstStartConfigErrorButton)
        mandatoryUpdateToolbarTitle = try values.decodeIfPresent(String.self, forKey: .mandatoryUpdateToolbarTitle)
        playServicesErrorToolbarTitle = try values.decodeIfPresent(String.self, forKey: .playServicesErrorToolbarTitle)
        appFirstStartConfigErrorToolbarTitle = try values.decodeIfPresent(String.self, forKey: .appFirstStartConfigErrorToolbarTitle)
    }

}

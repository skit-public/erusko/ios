//
//  AppConfig+RemoteKey.swift
//  eRusko
//
//  Created by Slovensko IT on 02/11/2020.
//

import Foundation

private typealias RemoteKey = AppConfig.RemoteKey

extension AppConfig {
    
    enum RemoteKey: String, CaseIterable {
        case exposuresData
        case exposuresServer
        case localizations
        case versionControl

        var keyValue: String {
            rawValue
        }

        var defaultValue: Any? {
            switch self {
            case .exposuresData:
                return ExposuresDataFBO.defaultValue
            case .localizations:
                return LocalizationFBO.defaultValue
            default:
                return nil
            }
        }

        private func localValue(forResource resource: String, withExtension extension: String, withKey key: String) -> String {
            guard
                let path = Bundle.main.url(forResource: resource, withExtension: `extension`),
                let dict = NSDictionary(contentsOf: path),
                let value = dict.value(forKey: key) as? String
                else {
                    log("Didn't found value for \(key)")
                    return ""
            }
            return value
        }

    }
}

//
//  AppConfig.swift
//  eRusko
//
//  Created by Slovensko IT on 30/10/2020.
//

import Foundation
import FirebaseRemoteConfig

struct AppConfig {
    
    static var shared = AppConfig()
    
    var exposuresData: ExposuresDataFBO = ExposuresDataFBO.defaultValue
    var exposuresServer: ExposuresServerFBO?
    var versionControl: VersionControlFBO?
    var localized: Localized = Localized()
    
    let showExposureForDays     = 14
    let shareAppDynamicLink     = "" //todo: link na zdielanie applikacie
    let defaultLanguage         = "SK"
    let unsupportedDeviceLink   = "https://korona.gov.sk"
    let minSupportedOSVersion   = Version("13.7")
    let appstoreLink            = "" //todo: link na appstore
    
    
    init() {
        setupDefaultValues()
        update()
    }
    
    public mutating func update() {
        exposuresData = parse(forKey: .exposuresData) ?? exposuresData
        exposuresServer = parse(forKey: .exposuresServer) ?? exposuresServer
        versionControl = parse(forKey: .versionControl) ?? versionControl
        
        setupLocalization()
    }
    
    private func setupDefaultValues() {
        var remoteDefaults: [String: NSObject] = [:]
        RemoteKey.allCases.forEach {
            remoteDefaults[$0.keyValue] = $0.defaultValue as? NSObject
        }
        RemoteConfig.remoteConfig().setDefaults(remoteDefaults)
    }
    
    private mutating func setupLocalization() {
        let identifier = Locale.preferredLanguages.first ?? defaultLanguage
        let lang = Locale(identifier: identifier).languageCode?.uppercased()
        
        let localizations: [LocalizationFBO]? = parse(forKey: .localizations)       

        if let localization = localizations?.first(where: { $0.locale == lang }) {
            localized = localization.texts ?? Localized()
            return
        }

        if let defaultLocalization = localizations?.first(where: { $0.locale == defaultLanguage }) {
            localized = defaultLocalization.texts ?? Localized()
        }
    }
    
    private func parse<T: Decodable>(forKey key: RemoteKey) -> T? {
        let jsonString = RemoteConfig.remoteConfig()[key.keyValue].stringValue ?? ""
        guard
            let json = jsonString.data(using: .utf8)
        else {
            log("return nil")
            return nil
        }
        do {
            let remoteContent = try JSONDecoder().decode(T.self, from: json)
            return remoteContent
        } catch {
            return nil
        }
    }
}

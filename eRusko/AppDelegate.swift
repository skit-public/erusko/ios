//
//  AppDelegate.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFunctions
import FirebaseRemoteConfig
import UserNotifications
import FirebaseCrashlytics

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    private var inBackgroundStage: Bool = false {
        didSet {
            Self.inBackground = inBackgroundStage
        }
    }

    private(set) static var inBackground: Bool = false
    
    var window: UIWindow?
    var mainRouter: MainRouter?
    
    static var shared: AppDelegate {
        UIApplication.shared.delegate as! AppDelegate
    }
    
    static let dependency = AppDependency()
    
    var openResultsCallback: (() -> Void)?
    var openSendReportsCallback: (() -> Void)?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        log("\n\n\n")
        log("-application didFinishLaunchingWithOptions--------------------------------\n")

        generalSetup()
        setupBackgroundSchedulingIfNeeded()
        setupMainRouter()
        FirebaseService.fetch(completion: { _ in 
            DispatchQueue.main.async {
                AppDelegate.shared.mainRouter?.updateInterface()
            }
        })
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        log("\n\n\n")
        log("-applicationDidBecomeActive---------------------------\n")
        setupBackgroundSchedulingIfNeeded()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        log("\n\n\n")
        log("-applicationDidEnterBackground---------------------------\n")

    }

    func applicationWillTerminate(_ application: UIApplication) {
        log("\n\n\n")
        log("-applicationWillTerminate----------------------------------\n")
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

    }

    func application(_ application: UIApplication, didReceiveRemoteNotification notification: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(.noData)
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {

        if let url = userActivity.webpageURL {
            let components = url.relativePath.components(separatedBy: "/")
            if components.contains("vc") {
                
                let code = url.lastPathComponent
                AppStorage.validationCodeFromSMS = code
                openSendReportsCallback?()
            }
        }
        return true
    }

}

extension AppDelegate: UNUserNotificationCenterDelegate {

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.notification.request.identifier {
        case BackgroundService.getNotificationIdentifier():
            openResultsCallback?()
        default:
            break
        }
        completionHandler()
    }

}

private extension AppDelegate {

    func generalSetup() {
        let center = UNUserNotificationCenter.current()
        center.delegate = self

        FirebaseApp.configure()
        FirebaseConfiguration.shared.setLoggerLevel(.min)
        Crashlytics.crashlytics().setCrashlyticsCollectionEnabled(true)

        Crashlytics.crashlytics().checkForUnsentReports { _ in
            Crashlytics.crashlytics().sendUnsentReports()
        }
    }
    
    func setupMainRouter() {
        let window = UIWindow()
        window.backgroundColor = .black
        window.makeKeyAndVisible()
        self.window = window
        
        let mainRouter = MainRouter()
        window.rootViewController = mainRouter.rootViewController
        
        self.mainRouter = mainRouter
        
    }

    func setupBackgroundSchedulingIfNeeded() {
        Self.dependency.background.scheduleBackgroundTaskIfNeeded()
        Self.dependency.background.scheduleExposurePausedStateBackgroundTaskIfNeeded()
    }

}

//
//  Date+Extensions.swift
//  eRusko
//
//  Created by Slovensko IT on 06/11/2020.
//

import Foundation

extension Date {


    func toStringDate() -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        
        return formatter.string(from: self)
    }
    
    func isBiggerThen(_ date: Date?) -> Bool {
        
        guard let date = date else { return true }
        
        let currentDateString = self.toStringDate()
        let comparingDateString = date.toStringDate()
        
        log(">> \(currentDateString)")
        log(">> \(comparingDateString)")

        if currentDateString.isSameAsStringNumber(comparingDateString) {
            return false
        } else {
            if currentDateString.isBiggerAsStringNumber(comparingDateString) {
                return true
            }
            return false
        }
    }
    
    static var riskyDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
        return dateFormatter
    }()
    
    func toLocalString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        let timeStamp = dateFormatter.string(from: self)
        return timeStamp
    }
}

//
//  UIColor+Extensions.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit

extension UIColor {   
    static var blackButtonBackground: UIColor = UIColor(named: "BlackButtonBackground")!
    static var blackButtonText: UIColor = UIColor(named: "BlackButtonText")!
    static var blueButtonBackground: UIColor = UIColor(named: "BlueButtonBackground")!
    static var blueButtonText: UIColor = UIColor(named: "BlueButtonText")!
    static var buttonBorder: UIColor = UIColor(named: "ButtonBorder")!
    static var navigationBarBackgroundColor: UIColor = UIColor(named: "navigationBarBackgroundColor")!
    static var navigationBarTextColor: UIColor = UIColor(named: "navigationBarTextColor")!
    static var redButtonBackground: UIColor = UIColor(named: "RedButtonBackground")!
    static var redButtonText: UIColor = UIColor(named: "RedButtonText")!
    static var riskyTableSeparatorColor: UIColor = UIColor(named: "riskyTableSeparatorColor")!
    static var screenBackgroundColor: UIColor = UIColor(named: "ScreenBackgroundColor")!
    static var screenTextColor: UIColor = UIColor(named: "ScreenTextColor")!
}

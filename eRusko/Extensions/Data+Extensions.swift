//
//  Data+Extensions.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import Foundation

extension Data {

    static func random(count: Int) -> Data {
        var result = Data(count: count)
        _ = result.withUnsafeMutableBytes {
            SecRandomCopyBytes(kSecRandomDefault, count, $0.baseAddress!)
        }
        return result
    }
}

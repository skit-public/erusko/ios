//
//  UIButton+Extensions.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit

extension UIButton {

    func setTitle(_ text: String) {
        setTitle(text, for: .normal)
    }

}

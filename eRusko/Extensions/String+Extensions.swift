//
//  String+Extensions.swift
//  eRusko
//
//  Created by Slovensko IT on 25/10/2020.
//

import UIKit

extension String {

    func attributedStringLinksColorized(_ links: [String]) -> NSAttributedString {

        let pattern = ["\\[.*?\\]"]

        let italicsRegex = try? NSRegularExpression(pattern: pattern[0], options: .allowCommentsAndWhitespace)

        let range = NSMakeRange(0, self.count)

        let italicsMatches = italicsRegex?.matches(in: self, options: .reportCompletion, range: range)

        let attributedText = NSMutableAttributedString(string: self)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17), range: range)

        var charactersRemovedFromString = 0

        let attributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 17),
                          NSAttributedString.Key.foregroundColor: UIColor.blue,
                          NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue] as [NSAttributedString.Key : Any]

        var i: Int = 0
        for match in italicsMatches! {

            let newRange = NSMakeRange(match.range.location - charactersRemovedFromString, match.range.length) // Take the updated range for when this loop iterates, otherwise this crashes.
            attributedText.addAttributes(attributes, range: newRange)
            attributedText.addAttribute(NSAttributedString.Key.link, value: links[i], range: newRange)
            i += 1

            let rangeOfFirstCharacter = NSMakeRange(match.range.location - charactersRemovedFromString, 1)

            attributedText.replaceCharacters(in: rangeOfFirstCharacter, with: "")

            charactersRemovedFromString += 2

            let rangeOfLastCharacter = NSMakeRange(match.range.location + match.range.length - charactersRemovedFromString, 1)

            attributedText.replaceCharacters(in: rangeOfLastCharacter, with: "")
        }

        return attributedText
    }

    func attributedStringBold(color: UIColor) -> NSAttributedString {

        let pattern = ["\\$\\$.*?\\$\\$"]

        let italicsRegex = try? NSRegularExpression(pattern: pattern[0], options: .allowCommentsAndWhitespace)

        let range = NSMakeRange(0, self.count)

        let italicsMatches = italicsRegex?.matches(in: self, options: .reportCompletion, range: range)

        let attributedText = NSMutableAttributedString(string: self)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17), range: range)

        var charactersRemovedFromString = 0

        let attributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 17),
                          NSAttributedString.Key.foregroundColor: color] as [NSAttributedString.Key : Any]

        for match in italicsMatches! {

            let newRange = NSMakeRange(match.range.location - charactersRemovedFromString, match.range.length) // Take the updated range for when this loop iterates, otherwise this crashes.
            attributedText.addAttributes(attributes, range: newRange)

            let rangeOfFirstCharacter = NSMakeRange(match.range.location - charactersRemovedFromString, 2)

            attributedText.replaceCharacters(in: rangeOfFirstCharacter, with: "")

            charactersRemovedFromString += 4

            let rangeOfLastCharacter = NSMakeRange(match.range.location + match.range.length - charactersRemovedFromString, 2)

            attributedText.replaceCharacters(in: rangeOfLastCharacter, with: "")
        }

        return attributedText
    }

    func isBiggerAsStringNumber(_ date: String) -> Bool {

        if self.compare(date, options: .numeric) == .orderedDescending {
            return true
        } else {
            return false
        }
    }

    func isSameAsStringNumber(_ date: String) -> Bool {

        if self.compare(date, options: .numeric) == .orderedSame {
            return true
        } else {
            return false
        }
    }
    
}


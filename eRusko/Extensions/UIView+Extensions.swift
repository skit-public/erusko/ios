//
//  UIView+Extensions.swift
//  eRusko
//
//  Created by Slovensko IT on 31/10/2020.
//

import Foundation
import UIKit

extension UIView {
        
    func dialogViewGraphicSetup() {
        
        let maskLayer = CAShapeLayer()
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 16, height: 16)).cgPath
        maskLayer.path = path
        self.layer.mask = maskLayer
        
        self.layer.shadowColor = UIColor(white: 0.5, alpha: 1).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 20
        self.layer.shadowPath = path
        self.layer.masksToBounds = false
    }
    
    func textFieldGraphicSetup() {
        
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 2
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.25
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10).cgPath
    }
}

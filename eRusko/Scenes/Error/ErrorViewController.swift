//
//  ErrorViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 14/10/2020.
//

import UIKit

class ErrorViewController: BaseViewController {
    
    var viewModel: ErrorViewModel?
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var headlineLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var cancelButton: RoundedButtonClear!
    @IBOutlet weak var submitButton: RoundedButtonFilled!
    @IBOutlet weak var iconImageView: UIImageView!
    
    
    
    static func instantiateViewController(with viewModel: ErrorViewModel) -> UIViewController? {
        let errorVC = StoryboardScene.Error.initialScene.instantiate()
        errorVC.viewModel = viewModel
        errorVC.modalPresentationStyle = .overCurrentContext
        return errorVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStrings()
    }
    
    func setupStrings() {
        
        headlineLabel.text = viewModel?.headline
        textLabel.text = viewModel?.text
        cancelButton.setTitle(viewModel?.cancelActionTitle ?? "", for: .normal)
        
        submitButton.setTitle(viewModel?.submitActionTitle ?? "", for: .normal)
        submitButton.isHidden = viewModel?.submitAction == nil
        
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dimIn()
    }
    
    @IBAction private func cancelButtonAction() {
        switch viewModel?.cancelAction {
        case .close:
            close()
        case .closeAndCustom(let customAction):
            closeWith(completion: customAction)
        default:
            break
        }
        dimOut()
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        
        switch viewModel?.submitAction {
        case .close:
            close()
        case .closeAndCustom(let customAction):
            closeWith(completion: customAction)
        default:
            break
        }
        dimOut()
    }
    
    @objc func close() {
        dismiss(animated: true)
    }

    func closeWith(completion: @escaping () -> Void) {
        dismiss(animated: true, completion: completion)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        containerView.dialogViewGraphicSetup()
    }
}

struct ErrorViewModel {
    
    let headline: String?
    let text: String?
    let cancelActionTitle: String?
    let cancelAction: ErrorCancelAction?
    let submitActionTitle: String?
    let submitAction: ErrorSubmitAction?

    enum ErrorCancelAction {
        case close
        case closeAndCustom(() -> Void)
    }
    
    enum ErrorSubmitAction {
        case close
        case closeAndCustom(() -> Void)
    }
    
    enum ErrorXAction {
        case close
        case closeAndCustom(() -> Void)
    }
    
    init(headline: String? = "",
         text: String? = "",
         cancelActionTitle: String? = "",
         cancelAction: (() -> Void)? = nil,
         submitActionTitle: String? = nil,
         submitAction: (() -> Void)? = nil) {
        self.init(headline: headline,
                  text: text,
                  cancelActionTitle: cancelActionTitle,
                  cancelAction: cancelAction == nil ? .close : .closeAndCustom(cancelAction!),
                  submitActionTitle: submitActionTitle,
                  submitAction: submitAction == nil ? nil : .closeAndCustom(submitAction!))
    }

    init(headline: String?,
         text: String?,
         cancelActionTitle: String?,
         cancelAction: ErrorCancelAction?,
         submitActionTitle: String?,
         submitAction: ErrorSubmitAction?) {
        self.headline = headline
        self.text = text
        self.cancelActionTitle = cancelActionTitle
        self.cancelAction = cancelAction
        self.submitActionTitle = submitActionTitle
        self.submitAction = submitAction
    }
}

extension ErrorViewModel {

    static let unknown = ErrorViewModel(
        headline: AppConfig.shared.localized.generalErrorTitle,
        text: AppConfig.shared.localized.generalErrorMessage,
        cancelActionTitle: AppConfig.shared.localized.generalClose,
        cancelAction: .close,
        submitActionTitle: nil,
        submitAction: nil
    )
}

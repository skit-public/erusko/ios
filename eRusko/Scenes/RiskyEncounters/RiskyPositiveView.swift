//
//  RiskyPositiveView.swift
//  eRusko
//
//  Created by Slovensko IT on 30/10/2020.
//

import UIKit

class RiskyPositiveView: UIView {
    
    
    @IBOutlet var titleLabel: UILabel!

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initSubViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initSubViews()
    }
    
    private func initSubViews() {

        self.backgroundColor = UIColor.screenBackgroundColor


    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleLabel.text = AppConfig.shared.localized.recentEncountersBody!

    }
}

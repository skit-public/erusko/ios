//
//  RiskyRS.swift
//  eRusko
//
//  Created by Slovensko IT on 25/10/2020.
//

import UIKit

class RiskyRSViewController: BaseViewController {
    
     
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var closeButton: Button!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var exposuresToShow: [Date] = []
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
        return dateFormatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView(frame: .zero)
        
        setupStrings()
        checkExposures()
    }
    
    func setupStrings() {
        
        titleLabel.text = AppConfig.shared.localized.riskyEncountersDialogTitle!
    }
    
    func checkExposures() {
        
        let showForDays = AppConfig.shared.showExposureForDays
        let exposures = AppStorage.exposures.sorted()
        if exposures.count > 0 {
            let showForDate = Calendar.current.date(byAdding: .day, value: -showForDays, to: Date()) ?? Date()
            exposuresToShow = exposures.filter({ (date) -> Bool in
                date > showForDate
            })
            
            tableView.reloadData()
            self.view.setNeedsLayout()
        } else {
            self.view.setNeedsLayout()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        containerView.dialogViewGraphicSetup()        
    }
    
    @IBAction func closeButtonAction(_ sender: Button) {
        
        dimOut()
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dimIn()
    }
}

extension RiskyRSViewController: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension RiskyRSViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exposuresToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "exposure", for: indexPath)
        cell.layer.backgroundColor = UIColor.clear.cgColor
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.text = dateFormatter.string(from: exposuresToShow[indexPath.row])
        
        return cell
    }
}

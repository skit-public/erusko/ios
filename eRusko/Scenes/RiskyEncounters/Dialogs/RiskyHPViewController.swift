//
//  aaa.swift
//  eRusko
//
//  Created by Slovensko IT on 24/10/2020.
//

import UIKit
import QuartzCore

class RiskyHPViewController: BaseViewController {
    
     
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var closeButton: Button!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var fourLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStrings()
    }
    
    func setupStrings() {
        
        titleLabel.text = AppConfig.shared.localized.mainSymptomsTitle!
        subtitleLabel.text = AppConfig.shared.localized.mainSymptomsSubtitle!
        
        firstLabel.text = AppConfig.shared.localized.mainSymptomsHighFever!
        secondLabel.text = AppConfig.shared.localized.mainSymptomsCoughing!
        thirdLabel.text = AppConfig.shared.localized.mainSymptomsSoreThroat!
        fourLabel.text = AppConfig.shared.localized.mainSymptomsBodyPain!
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        containerView.dialogViewGraphicSetup()        
    }
    
    @IBAction func closeButtonAction(_ sender: Button) {
        
        dimOut()
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dimIn()
    }
}

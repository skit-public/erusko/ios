//
//  aaa.swift
//  eRusko
//
//  Created by Slovensko IT on 24/10/2020.
//

import UIKit

class RiskyZZSViewController: BaseViewController {
    
     
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var closeButton: Button!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var fourLabel: UILabel!
    @IBOutlet weak var fiveLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStrings()
    }
    
    func setupStrings() {
        
        titleLabel.text = AppConfig.shared.localized.responsibleBehaviourTitle!
        subtitleLabel.text = AppConfig.shared.localized.responsibleBehaviourSubtitle!

        firstLabel.text = AppConfig.shared.localized.responsibleBehaviourWashHands!
        secondLabel.text = AppConfig.shared.localized.responsibleBehaviourDesinfectHands!
        thirdLabel.text = AppConfig.shared.localized.responsibleBehaviourCoughToCloth!
        fourLabel.text = AppConfig.shared.localized.responsibleBehaviourKeepDistance!
        fiveLabel.text = AppConfig.shared.localized.responsibleBehaviourWithFaceMask!
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        containerView.dialogViewGraphicSetup()
    }
    
    @IBAction func closeButtonAction(_ sender: Button) {
        
        dimOut()
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dimIn()
    }
}

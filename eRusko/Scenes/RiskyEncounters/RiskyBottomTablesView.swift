//
//  RiskyBottomTablesView.swift
//  eRusko
//
//  Created by Slovensko IT on 25/10/2020.
//

import UIKit

class RiskyBottomTablesView: UIView {
    
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet var container1TitleLabel: UILabel!
    @IBOutlet var container1subtitleLabel: UILabel!
    @IBOutlet var container1View: UIView!
    
    @IBOutlet var container2TitleLabel: UILabel!
    @IBOutlet var container2subtitleLabel: UILabel!
    @IBOutlet var container2View: UIView!
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    
    
    var button1ActionBlock: (() -> Void)?
    var button2ActionBlock: (() -> Void)?
    var button3ActionBlock: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initSubViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initSubViews()
    }
    
    private func initSubViews() {
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: Bundle(for: type(of: self)))
        nib.instantiate(withOwner: self, options: nil)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(containerView)
        self.addConstraints()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
                                        self.topAnchor.constraint(equalTo: containerView.topAnchor),
                                        self.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
                                        self.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
                                        self.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = UIColor.screenBackgroundColor

        container1View.layer.cornerRadius = 16
        container2View.layer.cornerRadius = 16
        
        container1TitleLabel.text = AppConfig.shared.localized.recentEncountersSymptomsTitle!
        container1subtitleLabel.text = AppConfig.shared.localized.recentEncountersSymptomsBody!
        
        container2TitleLabel.text = AppConfig.shared.localized.recentEncountersNoSymptomsTitle!
        container2subtitleLabel.text = AppConfig.shared.localized.recentEncountersNoSymptomsBody!
        
        button1.setTitle(AppConfig.shared.localized.recentEncountersMainSymptoms!)
        button2.setTitle(AppConfig.shared.localized.recentEncountersBehaviour!)
        button3.setTitle(AppConfig.shared.localized.recentEncountersRecent!)

    }
    
    @IBAction func button1Action(_ sender: UIButton) {
        if button1ActionBlock != nil {
            button1ActionBlock?()
        }
    }
    
    
    @IBAction func button2Action(_ sender: UIButton) {
        if button2ActionBlock != nil {
            button2ActionBlock?()
        }
    }
    
    
    @IBAction func button3Action(_ sender: UIButton) {
        if button3ActionBlock != nil {
            button3ActionBlock?()
        }
    }
    
    
}

//
//  RiskyNegativeView.swift
//  eRusko
//
//  Created by Slovensko IT on 25/10/2020.
//

import UIKit

class RiskyNegativeView: UIView {
    
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initSubViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initSubViews()
    }
    
    private func initSubViews() {

        self.backgroundColor = UIColor.screenBackgroundColor



    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleLabel.text = AppConfig.shared.localized.recentEncountersNoExposureTitle!
        subtitleLabel.text = AppConfig.shared.localized.recentEncountersNoExposureBody!
    }

}

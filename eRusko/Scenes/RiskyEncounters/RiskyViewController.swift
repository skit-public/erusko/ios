//
//  Risky.swift
//  eRusko
//
//  Created by Slovensko IT on 25/10/2020.
//

import UIKit

class RiskyViewController: BaseViewController, NavigationBarMethodsDelegate {
    
    func navigationBarBackButtonMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var bottomTableView: RiskyBottomTablesView!
    
    var riskyEncounterDate: Date = Date()
    var exposuresToShow: [Date] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupInterface()
    }
        
    func setupInterface() {
        
        bottomTableView.button1ActionBlock = { [weak self] in

            let vc = StoryboardScene.RiskyHP.initialScene.instantiate()
            vc.modalPresentationStyle = .overCurrentContext
            self?.navigationController?.present(vc, animated: true, completion: nil)
        }
        bottomTableView.button2ActionBlock = { [weak self] in

            let vc = StoryboardScene.RiskyZZS.initialScene.instantiate()
            vc.modalPresentationStyle = .overCurrentContext
            self?.navigationController?.present(vc, animated: true, completion: nil)
        }
        bottomTableView.button3ActionBlock = { [weak self] in

            let vc = StoryboardScene.RiskyRS.initialScene.instantiate()
            vc.modalPresentationStyle = .overCurrentContext
            self?.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
}

class RiskyViewControllerNegative: RiskyViewController {
    
    @IBOutlet weak var negativeView: RiskyNegativeView!
}

class RiskyViewControllerPositive: RiskyViewController {
    
    @IBOutlet weak var positiveView: RiskyPositiveView!
}

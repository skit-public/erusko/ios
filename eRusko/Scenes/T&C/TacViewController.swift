//
//  tacViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 03/11/2020.
//

import UIKit

class TacViewController: BaseViewController, NavigationBarMethodsDelegate {
    
    func navigationBarBackButtonMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStrings()
    }
    
    func setupStrings() {
        
        self.titleLabel.text = AppConfig.shared.localized.tacTitle
        self.textLabel.text = AppConfig.shared.localized.tacBody
    }
}

extension TacViewController : ScrollAble {
    var scrollingControl: UIScrollView {
        return self.scrollView
    }
}

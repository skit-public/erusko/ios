//
//  LogController.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit

final class LogController: UIViewController {

    // MARK: - Outlets

    @IBOutlet private weak var textView: UITextView!

    // MARK: - Properties

    private var logText: String = "" {
        didSet {
            textView.text = logText
        }
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Setup

    private func setup() {
        Log.delegate = self

        textView.text = ""
    }

    // MARK: -

    func purgeLog() {
        logText = ""
    }

}

extension LogController: LogDelegate {
    func didLog(_ text: String) {
        logToView(text)
    }
}

private extension LogController {
    static var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .medium
        return formatter
    }()

    private func logToView(_ text: String) {
        logText += "\n" + Self.formatter.string(from: Date()) + " " + text
    }
}

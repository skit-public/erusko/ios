//
//  FileLogController.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit

final class FileLogController: UIViewController {

    // MARK: - Outlets

    @IBOutlet private weak var textView: UITextView!

    private var timer: Timer?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        textView.text = FileLogger.shared.getLog()
    }

    deinit {
        timer?.invalidate()
    }

    // MARK: - Setup

    private func setup() {
        textView.text = ""

        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { [weak self] _ in
            self?.textView.text = FileLogger.shared.getLog()
        }
    }

    // MARK: -

    func purgeLog() {
        textView.text = ""
    }

}

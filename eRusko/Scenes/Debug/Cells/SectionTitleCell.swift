//
//  SectionTitleCell.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit

final class SectionTitleCell: UITableViewCell {

    static let identifier = "sectionTitleCell"

    @IBOutlet private weak var titleLabel: UILabel!

    func configure(for title: String) {
        titleLabel.text = title
    }
}

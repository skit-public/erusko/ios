//
//  HelpViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit

class HelpViewController: BaseViewController, NavigationBarMethodsDelegate {
    
    func navigationBarBackButtonMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupStrings()
    }
    
    func setupStrings() {

        self.titleLabel.text = AppConfig.shared.localized.aboutTitle
        self.textLabel.text = AppConfig.shared.localized.aboutBody
    }
}

extension HelpViewController : ScrollAble {
    var scrollingControl: UIScrollView {
        return self.scrollView
    }
}

//
//  Success.swift
//  eRusko
//
//  Created by Slovensko IT on 28/10/2020.
//

import UIKit


class SuccessViewController: BaseViewController {
    
    
    @IBOutlet weak var containerView: UIView!
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var okButton: RoundedButtonClear!
    
    var completionBlock: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupStrings()
    }
    
    func setupStrings() {
        
        titleLabel.text = AppConfig.shared.localized.submitKeysSuccessTitle
        descriptionLabel.text = AppConfig.shared.localized.submitKeysSuccessBody
        okButton.setTitle(AppConfig.shared.localized.generalOk!)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        containerView.dialogViewGraphicSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dimIn()
    }
    
    
    @IBAction func xButtonAction(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
        completionBlock?()
        dimOut()
    }
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
        completionBlock?()
        dimOut()
    }
    
}

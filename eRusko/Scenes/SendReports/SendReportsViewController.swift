//
//  SendReportsViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 14/10/2020.
//

import UIKit
import Reachability
import FirebaseAnalytics

class SendReportsViewController: BaseViewController, NavigationBarMethodsDelegate {
    
    func navigationBarBackButtonMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    var keys: [ExposureDiagnosisKey]? = nil
    var verificationPayload: String? = nil
    var hmacSecret: Data? = nil
    var traveler: Bool? = nil
    var token: String? = nil
    
    var isDeeplinkFlow = false
    
    private var code: String = ""
    private var isValid: Bool = false
        
    private var expirationSeconds: TimeInterval = 0
    private var expirationTimer: Timer?

    private var firstAppear: Bool = true
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var textFieldTitleLabel: UILabel!
    @IBOutlet weak var textFieldContainerView: TextFieldView!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var submitButton: RoundedButtonFilled!
    
    @IBOutlet weak var bottomViewContraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        submitButton.isEnabled = false

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        setupTextField()
        setupStrings()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if firstAppear {
            codeTextField.becomeFirstResponder()
            firstAppear = false
        }
        showTextFieldErrorVisual(false)
        handleSubmitButtonBasedOnValidation()
        
        if AppStorage.validationCodeFromSMS.count > 1 {
            
            isDeeplinkFlow = true

            self.code = AppStorage.validationCodeFromSMS
            self.codeTextField.text = self.code
            codeTextFieldChanged()
            AppStorage.validationCodeFromSMS = ""
        }
    }

    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        view.endEditing(true)
        if isDeeplinkFlow == true {
            Analytics.logEvent(ERA.app_vc_deeplink_opened.rawValue, parameters: nil)
        } else {
            Analytics.logEvent(ERA.app_vc_manually_typed.rawValue, parameters: nil)
        }
        self.report()
    }
    
    @IBAction private func resultAction() {
        let vc = StoryboardScene.Success.initialScene.instantiate()
        vc.modalPresentationStyle = .overCurrentContext
        vc.completionBlock = { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }

    @IBAction private func closeAction() {
        dismiss(animated: true)
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
}

extension SendReportsViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return validateTextChange(with: .code, textField: textField, changeCharactersIn: range, newString: string)
    }

}

private extension SendReportsViewController {

    // MARK: - Setup

    func setupTextField() {
        
        codeTextField.textContentType = .oneTimeCode
        codeTextField.addTarget(self, action: #selector(codeTextFieldChanged), for: .allEditingEvents)

    }
    
    @objc func codeTextFieldChanged() {
        
        codeTextField.text = codeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.code = codeTextField.text ?? ""
        
        showTextFieldErrorVisual(self.code.count == 0)

        handleSubmitButtonBasedOnValidation()

    }
    
    func handleSubmitButtonBasedOnValidation() {
        
        if let codeString = codeTextField.text {
            isValid = codeString.trimmingCharacters(in: .whitespacesAndNewlines).count > 4
            submitButton.isEnabled = isValid
        } else {
            isValid = false
            submitButton.isEnabled = false
        }
    }
    
    func showTextFieldErrorVisual(_ errorVisual: Bool) {
        
        if errorVisual {
            
            textFieldContainerView.layer.borderColor = UIColor.redButtonText.cgColor
            textFieldTitleLabel.textColor = UIColor.redButtonText
        } else {
            
            textFieldContainerView.layer.borderColor = UIColor.buttonBorder.cgColor
            textFieldTitleLabel.textColor = UIColor.buttonBorder
        }
    }

    func setupStrings() {

        textLabel.text = AppConfig.shared.localized.submitKeysBody!
        textFieldTitleLabel.text = AppConfig.shared.localized.submitKeysInputHint!
        submitButton.setTitle(AppConfig.shared.localized.submitKeysButton!)
    }

    // MARK: - Progress

    func reportShowProgress() {
        showProgress()

        isModalInPresentation = true
        navigationItem.setLeftBarButton(nil, animated: true)
    }

    func reportHideProgress() {
        hideProgress()

        isModalInPresentation = false
        navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(closeAction)), animated: true)
    }

    // MARK: - Reports

    enum ReportType {
        case normal, test
    }
    
    func report(traveler: Bool = false) {
        sendReport(with: .normal, traveler: traveler)
    }

    func sendReport(with type: ReportType, traveler: Bool) {

        let exposureService = AppDelegate.dependency.exposureService
        let callback: ExposureServicing.KeysCallback = { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .success(let keys):
                
                Analytics.logEvent(ERA.app_diagnostic_keys_shared.rawValue, parameters: nil)
                
                self.keys = keys
                self.traveler = traveler
                
                self.verifyCode()
            case .failure(let error):
                log("DataListVC: Failed to get exposure keys \(error)")
                self.reportHideProgress()
                
                if let nserror = error as? ExposureError {
                    switch nserror {
                    case .error(let errorIn):
                        if let nserrorIn = errorIn as? NSError,
                           nserrorIn.code == 4,
                           nserrorIn.domain == "ENErrorDomain" {
                                               
                           Analytics.logEvent(ERA.app_diagnostic_keys_permission_denied.rawValue, parameters: nil)
                        }
                        break
                    default:
                        break
                    }

                }
                
                switch error {
                case .noData:
                    self.resultAction()
                    self.showNoKeysError()
                default:
                    Analytics.logEvent(ERA.app_diagnostic_keys_error.rawValue, parameters: nil)
                    self.showSendDataError()
                }
            }
        }

        switch type {
        case .test:
            exposureService.getTestDiagnosisKeys(callback: callback)
        case .normal:
            exposureService.getDiagnosisKeys(callback: callback)
        }
    }

    func verifyCode() {
        
        guard let connection = try? Reachability().connection, connection != .unavailable else {
            showNoConnectionError(action: { [weak self] in
                self?.verifyCode()
            })
            return
        }
        reportShowProgress()

        AppDelegate.dependency.verification.verify(with: code) { [weak self] result in
            switch result {
            case .success(let token):
//                if #available(iOS 13.7, *) {
//                    self?.askIfUserIsTraveler(token: token)
//                } else {
                
                Analytics.logEvent(ERA.app_vc_verification_success.rawValue, parameters: nil)

                self?.token = token
                
                self?.requestCertificate()
//                }
            case .failure(let error):
                
                log("DataListVC: Failed to verify code \(error)")
                self?.reportHideProgress()
                
                if let verError = error as? ERError {
                    self?.showVerifyError(verError.getMessageBasedOnCode())
                    Analytics.logEvent(ERA.app_vc_verification_error.rawValue, parameters: ["error_code": verError.errorCode])
                } else {
                    self?.showVerifyError((AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!))
                }
            }
        }
    }
    
    func requestCertificate() {
        
        guard let connection = try? Reachability().connection, connection != .unavailable else {
            showNoConnectionError(action: { [weak self] in
                self?.requestCertificate()
            })
            return
        }
        
        let verificationService = AppDelegate.dependency.verification
        let reportService = AppDelegate.dependency.reporter
        
        guard let keys = self.keys, let token = self.token else { self.showSendDataError(); return }

        guard !keys.isEmpty else {
            self.reportHideProgress()
            self.resultAction()
            self.showNoKeysError()
            return
        }

        do {
            let secret = Data.random(count: 32)
            let hmacKey = try reportService.calculateHmacKey(keys: keys, secret: secret)
            verificationService.requestCertificate(token: token, hmacKey: hmacKey) { [weak self] result in
                
                guard let self = self else { return }
                
                switch result {
                case .success(let certificate):
                    
                    self.verificationPayload = certificate
                    self.hmacSecret = secret
                    
                    self.uploadKeys()
                case .failure(let error):
                    log("DataListVC: Failed to get verification payload \(error)")
                    self.reportHideProgress()
                    
                    if let verError = error as? ERError {
                        self.showCertificateError(verError.getMessageBasedOnCode())
                    } else {
                        self.showCertificateError((AppConfig.shared.localized.generalErrorTitle!, AppConfig.shared.localized.generalErrorMessage!))
                    }
                }
            }
        } catch {
            log("DataListVC: Failed to get hmac for keys \(error)")
            self.reportHideProgress()
            self.showSendDataError()
        }
    }

    func uploadKeys() {
        
        guard let connection = try? Reachability().connection, connection != .unavailable else {
            showNoConnectionError(action: { [weak self] in
                self?.uploadKeys()
            })
            return
        }
        
        guard let keys = self.keys, let verificationPayload = self.verificationPayload, let hmacSecret = self.hmacSecret, let traveler = self.traveler else { self.showSendDataError(); return }
        
        AppDelegate.dependency.reporter.uploadKeys(
            keys: keys,
            verificationPayload:
            verificationPayload,
            hmacSecret: hmacSecret,
            traveler: traveler) { [weak self] result in
            self?.reportHideProgress()
            
            switch result {
            case .success:
                self?.resultAction()
            case .failure(let error):
                
                if let verError = error as? ERError {
                    self?.showSendDataError(verError.getMessageBasedOnCode())
                } else {
                    self?.showSendDataError()
                }
            }
        }
    }

    func showVerifyError(_ errorMessage: (String, String)) {
        showAlert(
            title: errorMessage.0,
            message: errorMessage.1,
            cancelHandler: { [weak self] in
                self?.codeTextField.text = nil
                self?.codeTextField.becomeFirstResponder()
            }
        )
        showTextFieldErrorVisual(true)
    }
    
    func showCertificateError(_ errorMessage: (String, String)) {
        showAlert(
            title: errorMessage.0,
            message: errorMessage.1
        )
    }
    

    func showNoKeysError() {
        showAlert(
            title: AppConfig.shared.localized.generalErrorTitle!,
            message: AppConfig.shared.localized.submitKeysErrorNoKeys!
        )
    }

    func showSendDataError(_ errorMessage: (String, String)? = nil) {
        showAlert(
            title: errorMessage?.0 ?? AppConfig.shared.localized.generalErrorTitle!,
            message: errorMessage?.1 ?? AppConfig.shared.localized.generalErrorMessage!
        )
    }
    
    func showNoConnectionError(action: @escaping (() -> Void)) {
        showAlert(title: AppConfig.shared.localized.connectionErrorTitle!,
                  message: AppConfig.shared.localized.connectionErrorMessage!,
                  cancelTitle: AppConfig.shared.localized.generalClose!,
                  cancelHandler: { [weak self] in
                        self?.dismiss(animated: true, completion: nil)
                  },
                  submitAction: AlertAction(title: AppConfig.shared.localized.submitKeysTryAgain!,
                                            handler: { [weak self] in
                                                        self?.dismiss(animated: true) {
                                                            action()
                                                        }
                                                    }
                                            )
                )
    }
    
    @available (iOS 13.7, *)
    func askIfUserIsTraveler(token: String) {
        let exposureService = AppDelegate.dependency.exposureService
        exposureService.getUserTraveled { [weak self] result in
            switch result {
            case let .success(traveler):
                self?.report(traveler: traveler)
            case let .failure(error):
                #if DEBUG
                self?.show(error: error)
                #endif
                self?.report()
            }
        }
    }

}

extension SendReportsViewController {
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            bottomViewContraint.constant = keyboardHeight - view.safeAreaInsets.bottom + 20
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.view.layoutIfNeeded()
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        bottomViewContraint.constant = 0
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
}

extension SendReportsViewController : ScrollAble {
    var scrollingControl: UIScrollView {
        return self.scrollView
    }
}

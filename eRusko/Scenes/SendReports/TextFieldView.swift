//
//  TextFieldView.swift
//  eRusko
//
//  Created by Slovensko IT on 13/11/2020.
//

import UIKit

final class TextFieldView: UIView {
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.textFieldGraphicSetup()
    }
}

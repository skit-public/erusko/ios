//
//  AboutViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 14/10/2020.
//

import UIKit

class AboutViewController: BaseViewController, NavigationBarMethodsDelegate {
    
    func navigationBarBackButtonMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupStrings()
    }
    
    func setupStrings() {
        
        self.textLabel.text = AppConfig.shared.localized.infoBody ?? ""
    }
}

extension AboutViewController : ScrollAble {
    var scrollingControl: UIScrollView {
        return self.scrollView
    }
}


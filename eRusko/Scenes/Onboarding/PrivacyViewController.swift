//
//  PrivacyViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 14/10/2020.
//

import UIKit
import FirebaseAuth
import FirebaseAnalytics

let link: String = "podmienkyPouzivania"

class PrivacyViewController: BaseViewController, NavigationBarMethodsDelegate {
    
    func navigationBarBackButtonMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var submitButton: Button!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStrings()
    }
    
    func setupStrings() {

        titleLabel.text = AppConfig.shared.localized.privacyTitle
        descriptionTextView.attributedText = AppConfig.shared.localized.privacyBody!.attributedStringLinksColorized([link])
        descriptionTextView.delegate = self
        submitButton.setTitle(AppConfig.shared.localized.privacyButton!)
        submitButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 40)
        submitButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -15)
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        
        AppStorage.activated = true
        AppDelegate.shared.mainRouter?.updateInterface(animated: true)
        
        Analytics.logEvent(ERA.app_activation_finished.rawValue, parameters: nil)
    }
}

extension PrivacyViewController: UITextViewDelegate {

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        if URL.description == link {
            let vc = TemplateViewController.viewController(.TaC)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return false
    }
}

extension PrivacyViewController : OnboardingAnimatable {
    var animateControl: UIControl {
        return self.submitButton
    }
}

extension PrivacyViewController : ScrollAble {
    var scrollingControl: UIScrollView {
        return self.scrollView
    }
}

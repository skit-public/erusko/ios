//
//  OnboardingFrameAnimator.swift
//  eRusko
//
//  Created by Slovensko IT on 13/11/2020.
//

import UIKit

class OnboardingFrameAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    var showPush: Bool = false
    
    init(showPush: Bool = true) {
        self.showPush = showPush
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        if !showPush {
            animatePop(using: transitionContext)
            return
        }
        
        guard
            let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as? OnboardingAnimatable,
            let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as? OnboardingAnimatable
        else {
            return
        }
        
        fromVC.view.frame = transitionContext.finalFrame(for: toVC)
        transitionContext.containerView.insertSubview(toVC.view, aboveSubview: fromVC.view)
        
        //delay kvoli layoutSubviews
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            let fromFrame = fromVC.animateControl.frame
            let toFrame = toVC.animateControl.frame
            toVC.animateControl.frame = fromFrame
            
            UIView.animate(
                withDuration: self.transitionDuration(using: transitionContext),
                delay: 0,
                usingSpringWithDamping: 0.7,
                initialSpringVelocity: 0.5,
                options: [],
                animations: {
                    toVC.animateControl.frame = toFrame
                    for subview in fromVC.view.subviews {
                        subview.alpha = 0
                    }
                }, completion: { _ in
                    transitionContext.completeTransition(true)
                })
        }
    }

    func animatePop(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as? OnboardingAnimatable,
            let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as? OnboardingAnimatable
        else {
            return
        }

        let f = transitionContext.initialFrame(for: fromVC)
        let fOffPop = f.offsetBy(dx: f.width, dy: 0)
        fromVC.view.frame = fOffPop

        transitionContext.containerView.insertSubview(toVC.view, belowSubview: fromVC.view)
        for subview in toVC.view.subviews {
            subview.alpha = 1
        }
        let fromFrame = fromVC.animateControl.frame
        let toFrame = toVC.animateControl.frame
        toVC.animateControl.frame = fromFrame
        
        UIView.animate(
            withDuration: self.transitionDuration(using: transitionContext),
            delay: 0,
            usingSpringWithDamping: 0.7,
            initialSpringVelocity: 0.5,
            options: [],
            animations: {
                toVC.animateControl.frame = toFrame
            }, completion: { _ in
                transitionContext.completeTransition(true)
            })
    }
}

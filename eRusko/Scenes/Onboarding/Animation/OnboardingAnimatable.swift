//
//  OnboardingAnimatable.swift
//  eRusko
//
//  Created by Slovensko IT on 06/11/2020.
//

import UIKit

protocol OnboardingAnimatable : UIViewController {
    
    var animateControl: UIControl { get }
    var contentViewController: UIViewController { get }
}

extension OnboardingAnimatable {
    
    var contentViewController: UIViewController {
        return UIViewController()
    }
}

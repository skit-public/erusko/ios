//
//  FirstActivationViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 14/10/2020.
//

import UIKit
import UserNotifications
import FirebaseAnalytics
import FirebaseCrashlytics

let link1: String = "akoAppkaFunguje"
let link2: String = "oAplikacii"

class FirstActivationViewController: BaseViewController, UITextViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var submitButton: Button!
    
    var exposureNotificationAuthorized: Bool {
        AppDelegate.dependency.exposureService.authorizationStatus == .authorized
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupStrings()
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        
        Analytics.logEvent(ERA.app_activation_started.rawValue, parameters: nil)
        
        if self.exposureNotificationAuthorized {
            UNUserNotificationCenter.current().getNotificationSettings { [weak self] settings in
                DispatchQueue.main.async { [weak self] in
                    if settings.authorizationStatus == .notDetermined {
                        // Request authorization
                        self?.navigationController?.pushViewController(TemplateViewController.viewController(.ExposurePermissions), animated: true)

                    } else {
                        // Already authorized or denied
                        self?.navigationController?.pushViewController(TemplateViewController.viewController(.Privacy), animated: true)
                    }
                }
            }
        } else {
            self.navigationController?.pushViewController(TemplateViewController.viewController(.ExposurePermissions), animated: true)
        }
    }
    
    @IBAction func helpButtonAction(_ sender: UIButton) {
        
    }
    
    func setupStrings() {
        
        titleLabel.text = AppConfig.shared.localized.introTitle
        descriptionTextView.attributedText = AppConfig.shared.localized.introBody!.attributedStringLinksColorized([link1, link2])
        descriptionTextView.delegate = self
        if let submitTitle = AppConfig.shared.localized.introNextButton {
            submitButton.setTitle(submitTitle)
        }
        submitButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 40)
        submitButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -15)
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        if URL.description == link1 {
            let vc = TemplateViewController.viewController(.Help)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if URL.description == link2 {
            let vc = TemplateViewController.viewController(.About)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return false
    }

}

extension FirstActivationViewController : OnboardingAnimatable {
    var animateControl: UIControl {
        return self.submitButton
    }
}

extension FirstActivationViewController : ScrollAble {
    var scrollingControl: UIScrollView {
        return self.scrollView
    }
}

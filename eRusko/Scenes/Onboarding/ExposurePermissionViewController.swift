//
//  ExposurePermissionViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 14/10/2020.
//

import UIKit
import ExposureNotification
import UserNotifications
import FirebaseAnalytics

class ExposurePermissionViewController: BaseViewController, NavigationBarMethodsDelegate {
    
    func navigationBarBackButtonMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var submitButton: Button!
    
    var exposureService: ExposureServicing {
        AppDelegate.dependency.exposureService
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStrings()
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        requestExposurePermission()
    }
    
    func setupStrings() {
    
        titleLabel.text = AppConfig.shared.localized.activationNotificationsTitle
        descriptionLabel.text = AppConfig.shared.localized.activationNotificationsBody
        if let submitTitle = AppConfig.shared.localized.generalEnable {
            submitButton.setTitle(submitTitle)
        }
    }

    // MARK: - Request permission

    func requestExposurePermission() {
        self.exposureService.activate { [weak self] error in
            guard let self = self else { return }
            
            if let error = error {
                Analytics.logEvent(ERA.app_activation_permission_denied.rawValue, parameters: nil)
                log("ExposurePermissionVC: failed to active exposures \(error)")
                switch error {
                case .activationError(let code):
                    switch code {
                    case .notAuthorized:
                        self.showPermissionDeniedAlert(cancelAction: { [weak self] in
                            self?.navigationController?.popViewController(animated: true)
                        })
                    case .unsupported:
                        self.showUnsupported()
                    case .insufficientStorage, .insufficientMemory:
                        self.showExposureStorageError()
                    case .restricted, .notEnabled:
                        Analytics.logEvent(ERA.app_exposure_notifications_region_error.rawValue, parameters: nil)
                        self.showPermissionDeniedAlert(cancelAction: { [weak self] in
                            self?.requestNotificationPermission()
                        })
                    default:
                        self.showUnknownError(error, code: code)
                    }
                default:
                    self.showUnknownError(error)
                }
            } else {
                self.requestNotificationPermission()
            }
        }
    }

    func requestNotificationPermission() {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { [weak self] granted, _ in
                DispatchQueue.main.async { [weak self] in
                    if granted {
                        self?.navigationController?.pushViewController(TemplateViewController.viewController(.Privacy), animated: true)
                    } else {
                        self?.showPermissionDeniedAlert(cancelAction: { [weak self] in
                            self?.navigationController?.pushViewController(TemplateViewController.viewController(.Privacy), animated: true)
                        })
                    }
                }
            }
        )
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func showUnsupported() {
        
        let viewController = StoryboardScene.ForceUpdate.forceUpdateVC.instantiate()
        viewController.state = .ForceUpdateUnsupported
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    func openSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(settingsUrl) else { return }
        UIApplication.shared.open(settingsUrl)
    }

    func showExposureStorageError() {
        showAlert(title: AppConfig.shared.localized.exposureActivationStorageTitle!,
                  message: AppConfig.shared.localized.exposureActivationStorageBody!)
    }

    func showUnknownError(_ error: Error, code: ENError.Code = .unknown) {
        showAlert(
            title: AppConfig.shared.localized.generalErrorTitle!,
            message: AppConfig.shared.localized.generalErrorMessage!,
            cancelHandler: { [weak self] in self?.requestNotificationPermission() }
        )
    }

    func showPermissionDeniedAlert(cancelAction: @escaping () -> Void) {
        showAlert(
            title: AppConfig.shared.localized.exposureActivationRestrictedTitle!,
            message: AppConfig.shared.localized.exposureActivationRestrictedBody!,
            cancelTitle: AppConfig.shared.localized.generalClose!,
            cancelHandler: cancelAction,
            submitAction: (title: AppConfig.shared.localized.goToSettingsAction!, handler: { [weak self] in self?.openSettings() })
        )
    }
}

extension ExposurePermissionViewController : OnboardingAnimatable {
    var animateControl: UIControl {
        return self.submitButton
    }    
}


extension ExposurePermissionViewController : ScrollAble {
    var scrollingControl: UIScrollView {
        return self.scrollView
    }
}

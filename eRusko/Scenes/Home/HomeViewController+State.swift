//
//  HomeViewController+State.swift
//  eRusko
//
//  Created by Slovensko IT on 15/10/2020.
//

import UIKit

extension HomeViewController {
    
    enum State: String {
        case enabled
        case paused
        case disabledBluetooth = "disabled"
        case disabledExposures

        var color: UIColor {
            switch self {
            case .enabled:
                return UIColor.green //Asset.appEnabled.color
            case .paused:
                return UIColor.orange //Asset.appPaused.color
            case .disabledBluetooth, .disabledExposures:
                return UIColor.red //Asset.appDisabled.color
            }
        }

        var image: UIImage {
            switch self {
            case .enabled:
                return UIImage(named: "green_icon_active")!
            case .paused, .disabledBluetooth, .disabledExposures:
                return UIImage(named: "blue_icon_paused")!
            }
        }

        var headline: String {
            switch self {
            case .enabled:
                return AppConfig.shared.localized.dashboardTitleEnabled!
            case .paused:
                return AppConfig.shared.localized.dashboardTitleDisabled!
            case .disabledBluetooth:
                return AppConfig.shared.localized.exposureErrorFailedBluetoothDisabledTitle!
            case .disabledExposures:
                return AppConfig.shared.localized.exposureErrorFailedServiceDisabledTitle!
            }
        }

        var title: String? {
            switch self {
            case .enabled:
                return AppConfig.shared.localized.dashboardTitleDisabled
            default:
                return ""
            }
        }

        var text: String {
            switch self {
            case .enabled:
                return AppConfig.shared.localized.dashboardBody!
            case .paused:
                return AppConfig.shared.localized.dashboardBody!
            case .disabledBluetooth:
                return AppConfig.shared.localized.exposureErrorFailedBluetoothDisabledMessage!
            case .disabledExposures:
                return AppConfig.shared.localized.exposureErrorFailedServiceDisabledMessage!
            }
        }

        var actionTitle: String {
            switch self {
            case .enabled:
                return AppConfig.shared.localized.dashboardPauseApp!
            case .paused:
                return AppConfig.shared.localized.dashboardStartApp!
            case .disabledBluetooth:
                return AppConfig.shared.localized.generalEnable!
            case .disabledExposures:
                return AppConfig.shared.localized.generalEnable!
            }
        }
    }
}


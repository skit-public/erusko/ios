//
//  RiskyBanner.swift
//  eRusko
//
//  Created by Slovensko IT on 29/10/2020.
//

import UIKit

enum RiskyBannerState {
    
    case White(text: String)
    case Red(riskyCount: Int, text: String)
}

final class RiskyBanner: UIView {
    
    @IBOutlet weak var containerView: UIView!
    
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var arrowImageView: UIImageView!
    
    
    var tapAction: (() -> Void)?
    
    var state: RiskyBannerState = .White(text: "Vsekto je vporiadku") {
        didSet {
            switch state {
            case .White(let text):
                textLabel.attributedText = text.attributedStringBold(color: UIColor.blueButtonText)
                containerView.accessibilityLabel = text
                separatorView.backgroundColor = UIColor.blueButtonText
                containerView.backgroundColor = UIColor.blueButtonBackground
                textLabel.textColor = UIColor.blueButtonText
                arrowImageView.image = UIImage(named: "Arrow - Right 3blue")
            case .Red( _, let text):
                textLabel.attributedText = text.attributedStringBold(color: UIColor.redButtonText)
                containerView.accessibilityLabel = text
                separatorView.backgroundColor = UIColor.redButtonText
                containerView.backgroundColor = UIColor.redButtonBackground
                textLabel.textColor = UIColor.redButtonText
                arrowImageView.image = UIImage(named: "Arrow - Right 3red")
                break
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initSubViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initSubViews()
    }
    
    private func initSubViews() {
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: Bundle(for: type(of: self)))
        nib.instantiate(withOwner: self, options: nil)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(containerView)
        self.addConstraints()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
                                        self.topAnchor.constraint(equalTo: containerView.topAnchor),
                                        self.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
                                        self.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
                                        self.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
    
    
    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        
        tapAction?()
    }
    
    
}

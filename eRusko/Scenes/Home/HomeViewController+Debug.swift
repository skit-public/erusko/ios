//
//  HomeViewController+Debug.swift
//  eRusko
//
//  Created by Slovensko IT on 15/10/2020.
//

import UIKit
import Alamofire
import ExposureNotification
import FirebaseAuth

extension HomeViewController {
    
    // MARK: - Debug
    
    #if !APPSTORE || DEBUG
    func debugAction() {
        let controller = StoryboardScene.Debug.tabBar.instantiate()
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true)
    }

    func debugProcessReports() {
        showProgress()

        _ = self.reporter.downloadKeys(lastProcessedFileName: nil) { [weak self] result in
            switch result {
            case .success(let keys):
                self?.exposureService.detectExposures(
                    configuration: AppConfig.shared.exposuresData ,
                    URLs: keys.URLs
                ) { [weak self] result in
                    guard let self = self else { return }
                    self.hideProgress()

                    switch result {
                    case .success(var exposures):
                        exposures.sort { $0.date < $1.date }

                        guard !exposures.isEmpty else {
                            log("EXP: no exposures, skip!")
                            self.showAlert(title: "Exposures", message: "No exposures detected, device is clear.")
                            return
                        }

                        exposures.forEach { AppStorage.exposures.append($0.date) }
                        
                        self.checkExposuresAndShowBanner()

                    case .failure(let error):
                        self.show(error: error)
                    }
                }
            case .failure(let error):
                self?.hideProgress()
                self?.show(error: error)
            }
        }
    }

    func debugCancelRegistrationAction() {
        AppDelegate.dependency.exposureService.deactivate { _ in
            AppStorage.deleteAllData()
            AppDelegate.shared.mainRouter?.updateInterface()
        }
    }

    func debugInsertFakeExposure(_ lessThenTwoWeeks: Bool) {

        var randomDate = Date()
        if lessThenTwoWeeks {
            randomDate = generateRandomDate(daysBack: Int(arc4random() % 13))
        } else {
            randomDate = generateRandomDate(daysBack: Int(arc4random() % 100) + 15)
        }
        AppStorage.exposures.append(randomDate)

        let showForDays = AppConfig.shared.showExposureForDays
        let showForDate = Calendar.current.date(byAdding: .day, value: -showForDays, to: Date()) ?? Date()
            
            
        if randomDate.isBiggerThen(AppStorage.lastExposureWarningDate) && randomDate.isBiggerThen(showForDate) {
            AppDelegate.dependency.background.showExposureNotif()
            AppStorage.lastExposureWarningDate = Date()
        }
        
        self.checkExposuresAndShowBanner()
    }
    
    func generateRandomDate(daysBack: Int) -> Date {
        let day = arc4random_uniform(UInt32(daysBack))+1
        let hour = arc4random_uniform(23)
        let minute = arc4random_uniform(59)
        let today = Date(timeIntervalSinceNow: 0)
        let gregorian  = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        var offsetComponents = DateComponents()
        offsetComponents.day = -1 * Int(day - 1)
        offsetComponents.hour = -1 * Int(hour)
        offsetComponents.minute = -1 * Int(minute)
        let randomDate = gregorian?.date(byAdding: offsetComponents, to: today, options: .init(rawValue: 0) )
        return randomDate ?? Date()
    }
    
    func debugTimeTesting() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss"

        var smallerDate = dateFormatter.date(from: "01-01-2012 00:05:01")!
        var biggerDate  = dateFormatter.date(from: "03-12-2019 09:30:01")!

        log("\(smallerDate.isBiggerThen(biggerDate)) ->>> should be false")
        
        smallerDate = dateFormatter.date(from: "21-01-2019 00:05:01")!
        biggerDate  = dateFormatter.date(from: "03-12-2019 09:30:01")!

        log("\(smallerDate.isBiggerThen(biggerDate)) ->>> should be false")
        
        smallerDate = dateFormatter.date(from: "01-12-2019 00:05:01")!
        biggerDate  = dateFormatter.date(from: "03-12-2019 09:30:01")!

        log("\(smallerDate.isBiggerThen(biggerDate)) ->>> should be false")
        
        smallerDate = dateFormatter.date(from: "03-12-2019 11:05:01")!
        biggerDate  = dateFormatter.date(from: "03-12-2019 09:30:01")!

        log("\(smallerDate.isBiggerThen(biggerDate)) ->>> should be the same, false")
        
        smallerDate = dateFormatter.date(from: "03-12-2019 11:05:01")!
        biggerDate  = dateFormatter.date(from: "03-12-2019 09:30:01")!

        log("\(smallerDate.isBiggerThen(nil)) ->>> day missing, should be true")
        
        smallerDate = dateFormatter.date(from: "21-01-2029 00:05:01")!
        biggerDate  = dateFormatter.date(from: "03-12-2019 09:30:01")!

        log("\(smallerDate.isBiggerThen(biggerDate)) ->>> should be true")
        
        smallerDate = dateFormatter.date(from: "01-12-2029 00:05:01")!
        biggerDate  = dateFormatter.date(from: "03-12-2019 09:30:01")!

        log("\(smallerDate.isBiggerThen(biggerDate)) ->>> should be true")
        
        smallerDate = dateFormatter.date(from: "03-12-2029 11:05:01")!
        biggerDate  = dateFormatter.date(from: "03-12-2019 09:30:01")!

        log("\(smallerDate.isBiggerThen(biggerDate)) ->>> should be true")
        
        smallerDate = dateFormatter.date(from: "13-12-2019 11:05:01")!
        biggerDate  = dateFormatter.date(from: "03-12-2019 09:30:01")!

        log("\(smallerDate.isBiggerThen(biggerDate)) ->>> should be true")
        
        smallerDate = dateFormatter.date(from: "03-12-2019 11:05:01")!
        biggerDate  = dateFormatter.date(from: "06-10-2019 09:30:01")!

        log("\(smallerDate.isBiggerThen(biggerDate)) ->>> should be true")
        
        smallerDate = dateFormatter.date(from: "04-12-2018 11:05:01")!
        biggerDate  = dateFormatter.date(from: "03-10-2019 09:30:01")!

        log("\(smallerDate.isBiggerThen(biggerDate)) ->>> should be false")
    }

    #endif
}

//
//  HomeViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit
import Alamofire
import ExposureNotification
import FirebaseAuth
import FirebaseAnalytics

class HomeViewController: BaseViewController, NavigationBarMethodsDelegate {
    
    func navigationBarRightButtonMethod() {
        self.shareAppAction()
        Analytics.logEvent(ERA.app_share_app_clicked.rawValue, parameters: nil)
    }
    
    var state: State = .disabledExposures {
        didSet {
            self.setupStrings()
        }
    }
    var exposureToShow: Date?

    let exposureService: ExposureServicing = AppDelegate.dependency.exposureService
    let reporter: ReportServicing = AppDelegate.dependency.reporter
    let backgroundService = AppDelegate.dependency.background
    
    var exposureTitle: String {
        AppConfig.shared.localized.notificationExposedBody!
    }

    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yy"
        return formatter
    }()
    
    
    
    @IBOutlet private weak var exposureBannerView: RiskyBanner!

    @IBOutlet private weak var mainStackView: UIStackView!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var headlineLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var textLabel: UILabel!
    @IBOutlet private weak var actionButton: Button!
    @IBOutlet weak var sendReportsButton: SendReportsButton!
    @IBOutlet weak var footerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        exposureBannerView.tapAction = { [weak self] in

            self?.riskyEncountersAction()
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.enManagerStateChanged),
            name: NSNotification.Name(rawValue: "ENManagerStateChanged"),
            object: nil)
        
        checkExposuresAndShowBanner()

        AppDelegate.shared.openResultsCallback = { [weak self] in
            self?.riskyEncountersAction()
        }
        
        AppDelegate.shared.openSendReportsCallback = { [weak self] in
            self?.showSendReportsActionWithSMSIfNeeded()
        }    
        
        #if !APPSTORE || DEBUG
        let pan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(debugMenuAction(_:)))
        pan.edges = .right
        self.view.addGestureRecognizer(pan)
        self.footerView.addGestureRecognizer(pan)
        #endif
        
        if AppStorage.startedScanningAfterFirstRun != true {
            resumeScanning()
            AppStorage.startedScanningAfterFirstRun = true
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(
            self,
            name: NSNotification.Name(rawValue: "ENManagerStateChanged"),
            object: nil)
        NotificationCenter.default.removeObserver(
            self,
            name: UIApplication.didBecomeActiveNotification,
            object: nil
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateStateIfNeeded()
        view.setNeedsLayout()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidBecomeActive),
            name: UIApplication.didBecomeActiveNotification,
            object: nil
        )

        checkBackgroundModeIfNeeded()
        
        showSendReportsActionWithSMSIfNeeded()
    }
    
    func showSendReportsActionWithSMSIfNeeded() {
        if AppStorage.validationCodeFromSMS.count > 1 {
            sendReportsAction()
        }
    }
    
    func checkExposuresAndShowBanner() {
        
        let showForDays = AppConfig.shared.showExposureForDays
        let exposures = AppStorage.exposures.sorted()
        if exposures.count > 0 {
            let showForDate = Calendar.current.date(byAdding: .day, value: -showForDays, to: Date()) ?? Date()
            exposureToShow = exposures.last(where: { $0 > showForDate })
            if exposureToShow == nil {
                exposureBannerView.state = .White(text: AppConfig.shared.localized.dashboardBannerNoContactLately!)
            } else {
                exposureBannerView.state = .Red(riskyCount: 10, text: AppConfig.shared.localized.dashboardBannerContact!.replacingOccurrences(of: "%s", with: "$$\(self.dateFormatter.string(from: exposureToShow!))$$"))
            }
            self.view.setNeedsLayout()
        } else {
            exposureBannerView.state = .White(text: AppConfig.shared.localized.dashboardBannerNoContactLately!)
            self.view.setNeedsLayout()
        }
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else { return }
//        exposureBannerView.layer.shadowColor = self.cardShadowColor(traitCollection: traitCollection)
    }
    
    private func pauseScanning() {
        
        updateScanner(activate: false) { [weak self] in
            self?.updateStateIfNeeded()
        }
    }

    private func resumeScanning() {
        
        updateScanner(activate: true) { [weak self] in
           self?.updateStateIfNeeded()
        }
    }
    
    /* zdileni aplikacie, tlacitko hore v navigation bare "sdilet aplikaci" */
    private func shareAppAction() {
        guard let url = URL(string: AppConfig.shared.shareAppDynamicLink) else { return }

        let shareContent: [Any] = ["\(AppConfig.shared.localized.dashboardShareText!) \(url.absoluteString)"]
        let activityViewController = UIActivityViewController(activityItems: shareContent, applicationActivities: nil)
        activityViewController.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItems?.last ?? navigationItem.rightBarButtonItem

        present(activityViewController, animated: true)
    }

    /* submit button action.. naspodu tlacitko "pozastavit" */
    @IBAction private func changeScanningAction() {
        
        switch self.state {
        case .enabled:
            pauseScanning()
        case .paused:
            resumeScanning()
        case .disabledBluetooth:
            openBluetoothSettings()
        case .disabledExposures:
            if self.exposureService.authorizationStatus == .unknown {
                resumeScanning()
            } else {
                openSettings()
            }
        }
    }

    @IBAction func debugMenuAction(_ sender: Any) {
        #if !APPSTORE || DEBUG
        
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        controller.addAction(UIAlertAction(title: "Debug", style: .default, handler: { [weak self] _ in
            self?.debugAction()
        }))
        controller.addAction(UIAlertAction(title: "Debug error", style: .default, handler: { [weak self] _ in
            
            let viewModel = ErrorViewModel()
            if let errorVC = ErrorViewController.instantiateViewController(with: viewModel) {
                self?.navigationController?.present(errorVC, animated: true)
            }
        }))
        controller.addAction(UIAlertAction(title: "Debug clear eTags", style: .default, handler: { _ in
            
            AppStorage.eTagsList = []
        }))
        controller.addAction(UIAlertAction(title: "Debug aktivacia", style: .default, handler: { [weak self] _ in
            self?.debugCancelRegistrationAction()
        }))
        controller.addAction(UIAlertAction(title: "Debug rizikove stretnutie < 14 dni", style: .default, handler: { [weak self] _ in
            self?.debugInsertFakeExposure(true)
        }))
        controller.addAction(UIAlertAction(title: "Debug rizikove stretnutie > 14 dni", style: .default, handler: { [weak self] _ in
            self?.debugInsertFakeExposure(false)
        }))
        controller.addAction(UIAlertAction(title: "Debug zkontrolovat reporty", style: .default, handler: { [weak self] _ in
            self?.debugProcessReports()
        }))
        
        controller.addAction(UIAlertAction(title: "Zatvorit", style: .cancel))
        present(controller, animated: true)
        
        #endif
    }

    @IBAction private func exposureMoreInfo(_ sender: Any) {
        riskyEncountersAction()
    }
    
    @IBAction func riskyEncountersBarButtonAction(_ sender: UIButton) {
        riskyEncountersAction()
    }
    
    @IBAction func sendReportsBarButtonAction(_ sender: UIButton) {
        
        sendReportsAction()
    }
    
    private func sendReportsAction() {
        #if targetEnvironment(simulator)
            let vc = TemplateViewController.viewController(.SendReports)
            self.navigationController?.pushViewController(vc, animated: true)
        #else
        if self.exposureService.status != .active &&
            self.exposureService.status != .paused &&
            self.exposureService.status != .disabled &&
            self.exposureService.status != .bluetoothOff {
            showAlert(
                title: AppConfig.shared.localized.submitKeysErrorRestrictionTitle!,
                message: AppConfig.shared.localized.submitKeysErrorRestrictionMessage!,
                cancelTitle: AppConfig.shared.localized.generalClose!,
                submitAction: (AppConfig.shared.localized.generalEnable!, { [weak self] in self?.openSettings() }) //gotosettings
            )
        } else {
            let vc = TemplateViewController.viewController(.SendReports)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        #endif

    }
    
    private func riskyEncountersAction() {
        
        let vc: UIViewController
        var exposuresToShow: [Date] = []

        let showForDays = AppConfig.shared.showExposureForDays
        let exposures = AppStorage.exposures.sorted()
        if exposures.count > 0 {
            exposuresToShow = exposures
            let showForDate = Calendar.current.date(byAdding: .day, value: -showForDays, to: Date()) ?? Date()
            if let riskyEncounterDateToShow = exposures.last(where: { $0 > showForDate }) {
                
                vc = TemplateViewController.viewController(.RiskyEncountersPositive, dateToShow: riskyEncounterDateToShow, exposuresToShow: exposuresToShow)
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
            vc = TemplateViewController.viewController(.RiskyEncountersNegative)
        } else {
            vc = TemplateViewController.viewController(.RiskyEncountersNegative)
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func cardShadowColor(traitCollection: UITraitCollection) -> CGColor {
        return UIColor.label.resolvedColor(with: traitCollection).withAlphaComponent(0.2).cgColor
    }
    
    @objc private func applicationDidBecomeActive() {
        updateStateIfNeeded()
    }

    @objc private func enManagerStateChanged(notification: NSNotification) {
       updateStateIfNeeded()
    }    

    func updateStateIfNeeded() {

        var statusAnalytics: String = "Inactive"
        switch exposureService.status {
        case .active:
            state = .enabled
            statusAnalytics = "Active"
        case .paused, .disabled, .unauthorized:
            state = .paused
        case .bluetoothOff:
            state = .disabledBluetooth
        case .restricted:
            state = .disabledExposures
        case .unknown:
            state = .paused
        @unknown default:
            return
        }
        Analytics.setUserProperty(statusAnalytics, forName: ERA.app_exposure_state.rawValue)
    }
}


private extension HomeViewController {

    func updateScanner(activate: Bool, completion: @escaping () -> Void) {
        if activate {
            self.exposureService.activate { [weak self] error in
                guard let self = self else { return }

                if let error = error {
                    switch error {
                    case .activationError(let code):
                        switch code {
                        case .notAuthorized, .notEnabled:
                            self.showAlert(
                                title: AppConfig.shared.localized.exposureActivationRestrictedTitle!,
                                message: AppConfig.shared.localized.exposureActivationRestrictedBody!,
                                cancelTitle: AppConfig.shared.localized.generalClose!,
                                cancelHandler: nil,
                                submitAction: (title: AppConfig.shared.localized.goToSettingsAction!, handler: { [weak self] in self?.openSettings() })
                            )
                        case .unsupported:
                            self.exposureService.deactivate { [weak self] _ in
                                self?.exposureService.activate { [weak self] error in
                                    guard let error = error else { return }
                                    switch error {
                                    case .activationError(let code):
                                        self?.showExposureUnknownError(error, code: code, activation: true)
                                    default:
                                        self?.showExposureUnknownError(error, activation: true)
                                    }
                                }
                            }
                        case .restricted:
                            self.showAlert(
                                title: AppConfig.shared.localized.exposureActivationRestrictedTitle!,
                                message: AppConfig.shared.localized.exposureActivationRestrictedBody!,
                                cancelTitle: AppConfig.shared.localized.generalClose!,
                                cancelHandler: nil,
                                submitAction: (title: AppConfig.shared.localized.goToSettingsAction!, handler: { [weak self] in self?.openSettings() })
                            )
                        case .insufficientStorage, .insufficientMemory:
                            self.showExposureStorageError()
                        default:
                            self.showExposureUnknownError(error, code: code, activation: true)
                        }
                    default:
                        self.showExposureUnknownError(error, activation: true)
                    }
                    log("ActiveAppVC: failed to active exposures \(error)")
                }
                completion()
            }
        } else {
            self.exposureService.deactivate { [weak self] error in
                if let error = error {
                    self?.showExposureUnknownError(error, activation: false)
                    log("ActiveAppVC: failed to disable exposures \(error)")
                }
                completion()
            }
        }
    }

    func setupStrings() {

        imageView.image = self.state.image
        headlineLabel.text = self.state.headline
        sendReportsButton.setTitle(AppConfig.shared.localized.dashboardSubmitButton!)
        textLabel.text = self.state.text

        UIView.transition(with: actionButton, duration: 0.5, options: .transitionCrossDissolve, animations: { [weak self] in
            self?.actionButton.setTitle(self?.state.actionTitle ?? "")
            self?.actionButton.style = self?.state == .enabled ? .filled : .filled
            if self?.state == .enabled {
                self?.actionButton.setImage(UIImage(named: "mdi_pause"), for: .normal)
            } else {
                self?.actionButton.setImage(nil, for: .normal)

            }
        }, completion: nil)
    }

    func checkBackgroundModeIfNeeded() {
        guard !AppStorage.backgroundModeAlertShown, UIApplication.shared.backgroundRefreshStatus == .denied else { return }
        AppStorage.backgroundModeAlertShown = true
        let controller = UIAlertController(
            title: AppConfig.shared.localized.activeBackgroundModeTitle!,
            message: AppConfig.shared.localized.activeBackgroundModeMessage!,
            preferredStyle: .alert
        )
        controller.addAction(UIAlertAction(title: AppConfig.shared.localized.goToSettingsAction!, style: .default, handler: { [weak self] _ in
            self?.openSettings()
        }))
        controller.addAction(UIAlertAction(title: AppConfig.shared.localized.generalClose, style: .default))
        controller.preferredAction = controller.actions.first
        present(controller, animated: true)
    }

    func showExposureStorageError() {
        showAlert(
            title: AppConfig.shared.localized.exposureActivationStorageTitle!,
            message: AppConfig.shared.localized.exposureActivationStorageBody!
        )
    }

    func showExposureUnknownError(_ error: Error, code: ENError.Code = .unknown, activation: Bool) {
        if activation {
            showAlert(
                title: AppConfig.shared.localized.exposureActivationUnknownTitle!, //exposure_activation_unknown_title
                message: AppConfig.shared.localized.exposureActivationUnknownBody! //exposure_activation_unknown_body
            )
        } else {
            showAlert(
                title: AppConfig.shared.localized.exposureDeactivationUnknownTitle!, //exposure_deactivation_unknown_title
                message: AppConfig.shared.localized.exposureDeactivationUnknownBody! //exposure_deactivation_unknown_body
            )
        }
    }

    // MARK: - Open external

    func openSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(settingsUrl) else { return }
        UIApplication.shared.open(settingsUrl)
    }

    func openBluetoothSettings() {
        let url: URL?
        if !self.exposureService.isBluetoothOn {
            url = URL(string: UIApplication.openSettingsURLString)
        } else {
            url = URL(string: UIApplication.openSettingsURLString)
        }

        guard let URL = url, UIApplication.shared.canOpenURL(URL) else { return }
        UIApplication.shared.open(URL)
    }
}

//
//  IntroViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 13/10/2020.
//

import UIKit

class IntroViewController: UIViewController {
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
       
    }
}


//
//  ForceUpdateViewController.swift
//  eRusko
//
//  Created by Slovensko IT on 14/10/2020.
//

import UIKit

class ForceUpdateViewController: BaseViewController {
    
    var state: TemplateControllerState?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var submitButton: Button!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStrings()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setupStrings()
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        switch state {
        case .ForceUpdateMandatory:
            UIApplication.shared.open(URL(string: AppConfig.shared.appstoreLink)!) 
        case .ForceUpdateOS:
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        case .ForceUpdateUnsupported:
            guard let url = URL(string: AppConfig.shared.unsupportedDeviceLink) else { return } 
            openURL(URL: url)
        case .ForceUpdateRetryConnection:
            submitButton.isEnabled = false
            activityIndicator.startAnimating()
            FirebaseService.fetch(completion: { [weak self] _ in
                guard let self = self else { return }

                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.submitButton.isEnabled = true
                    
                    if !FirebaseService.needFetch {
                        AppDelegate.shared.mainRouter?.updateInterface(animated: false)
                    }
                }                
            })
        default: break
            
        }
    }
    
    private func setupStrings() {

        switch state {
        case .ForceUpdateMandatory:
            titleLabel.text = AppConfig.shared.localized.mandatoryUpdateSubtitle
            descriptionLabel.text = AppConfig.shared.localized.mandatoryUpdateBody
            submitButton.setTitle((AppConfig.shared.localized.mandatoryUpdateButton)!)
            break
        case .ForceUpdateOS:
            titleLabel.text = AppConfig.shared.localized.forceOsUpdateSubtitle
            descriptionLabel.text = AppConfig.shared.localized.forceOsUpdateBody
            submitButton.setTitle((AppConfig.shared.localized.mandatoryUpdateButton)!)
            break
        case .ForceUpdateUnsupported:
            titleLabel.text = AppConfig.shared.localized.unsupportedDeviceSubtitle
            descriptionLabel.text = AppConfig.shared.localized.unsupportedDeviceBody
            submitButton.setTitle((AppConfig.shared.localized.unsupportedDeviceButton)!)
            break
        case .ForceUpdateRetryConnection:
            titleLabel.text = AppConfig.shared.localized.appFirstStartConfigErrorTitle
            descriptionLabel.text = AppConfig.shared.localized.appFirstStartConfigErrorBody
            submitButton.setTitle((AppConfig.shared.localized.appFirstStartConfigErrorButton)!)
            break
        default: break
            
        }
    }
}

extension ForceUpdateViewController : OnboardingAnimatable {
    var animateControl: UIControl {
        return self.submitButton
    }
}
